-------------------------------------------------------------------------------
-- Read from a data file as input to simulation, using testio lib
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use WORK.deapTrigConst.all;

library std;
use std.textio.all;

--~/work/deap/firmware/deap_vhdl/sim.dat
--~/work/deap/firmware/deap_vhdl/tmp.dat
--~/home/amuir/work/deap/firmware/trunk/deap_vhdl/tmp500Evts.dat
-- AAA-03F-AAA
--~/home/amuir/work/deap/firmware/trunk/deap_vhdl/oneTest.dat

entity rdFile is
  port (
    FakeDataEn : in  std_logic;
    BClk300    : out std_logic;
    WClk50     : out std_logic;
    DataOut    : out std_logic_vector(DataWidth-1 downto 0)
    );
end rdFile;

architecture Behavioral of rdFile is

-- Generate BCLK (300MHz) and WCLK (50MHz)
  signal iBClk300 : std_logic := '0';
  signal iWClk50  : std_logic := '0';
  signal strtWClk : std_logic := '0';

  signal iDataOut    : std_logic_vector(DataWidth-1 downto 0) := (others => '0');
  signal EndOFEvt    : lword                                  := X"00000000";
  signal readCnt     : lword                                  := (others => '0');
  signal iFakeDataEn : std_logic                              := '0';
  signal alignCnt    : byte := X"00";
  signal strtDelay   : std_logic                              := '0';
  signal delayCnt    : lword                                  := X"00000000";
  
begin  -- Behavioral

  BClk300     <= iBClk300;
  WClk50      <= iWClk50;

  ---- WClk 50
  wclock50 : process
  begin
    if(strtWClk = '0') then
      strtWClk <= '1';
      wait for 30 ns;
      iWClk50 <= not iWClk50;
    else
      wait for 60 ns;
      iWClk50 <= not iWClk50;
    end if;
  end process wclock50;

  ---- BClk 300
  bclock300 : process
  begin
    wait for 10 ns;
    iBClk300 <= not iBClk300;
  end process bclock300;

-- Align data to WCLK
  align : process(iBClk300, iWClk50, alignCnt, iFakeDataEn)
    begin
      if((rising_edge(iBClk300) or falling_edge(iBClk300)) and FakeDataEn = '1' and iFakeDataEn = '0') then
        if(rising_edge(iWClk50)) then
          alignCnt <= (others => '0');
        end if;
        alignCnt <= alignCnt + 1;        
        if(alignCnt = 28) then
          iFakeDataEn <= FakeDataEn;
        end if;
      end if;
    end process align;
  
  fileIn : process (iBClk300, iFakeDataEn, strtDelay)
-- Input file signals
    file simFileIn  : text;
    variable L      : line;
    variable R      : bit_vector(31 downto 0);
    variable edFile : boolean          := false;
    variable good   : boolean          := true;
    variable ok     : file_open_status := name_error;
  begin  -- process fileIn
    
    if rising_edge(iBClk300) or falling_edge(iBClk300) then
-- default
      if(EndOfEvt(0) = '0') then 
        DataOut <= iDataOut;
      else
        DataOut <= (others => '0');
      end if;
        
-- delay between evts
      if (strtDelay = '1') then
        delayCnt <= delayCnt + 1;
        if(delayCnt = X"9C4") then
          strtDelay <= '0';
          delayCnt  <= (others => '0');
        end if;
      end if;

-- Enable fake data
      if(iFakeDataEn = '1' and strtDelay = '0') then

-- open input file      
        if ok /= open_ok then
          file_open(ok, simFileIn, "/home/amuir/work/deap/firmware/trunk/deap_vhdl/oneHit.dat", read_mode);
        elsif (not edFile) then
          edFile := endfile(simFileIn);
          if(EndOfEvt(0) = '1') then
            strtDelay <= '1';
            EndOfEvt  <= (others => '0');
          elsif (not endfile(simFileIn) and ok = open_ok) then
            readline(simFileIn, L);
            read(L, R, good);
            if good then
              iDataOut <= To_StdLogicVector(R(31 downto 8));
              EndOfEvt <= To_StdLogicVector(R);              
            end if;
            readCnt <= readCnt + 1;
          end if;
        end if;
-- Finished reading so close file
        if edFile then
          file_close(simFileIn);
          iDataOut <= (others => '0');          
        end if;
      end if;

    end if;  --   if(iFakeDataEn = '1' and strtDelay = '0') then
    
  end process fileIn;
  
end Behavioral;

