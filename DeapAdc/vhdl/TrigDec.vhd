-------------------------------------------------------------------------------
-- Trigger decision
-- Inputs: energy sums and potentially geographical
--         clk
--
-- Outputs: trigger decision
--        : trigger type
--        : trigger number
--
-- Description:
-- Calculate Fprmt
-- trigger
-- decide trigger type and number
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use WORK.deapTrigConst.all;

entity TrigDec is

  port (
    Clk            : in std_logic;
    Rst            : in std_logic;
    Sum300_0       : in std_logic_vector(21 downto 0);
    Sum300_48      : in std_logic_vector(21 downto 0);    
    Sum5us_0       : in std_logic_vector(24 downto 0);
    TrigCR         : in lword;
    EnrgyThres1    : in lword;
    EnrgyThres2    : in lword;
    FPrmptThres1   : in lword;
    FPrmptThres2   : in lword;
    -- Fake software trigger
    TrigSoft       : in lword;
    -- Read trigger information into memory
    TrigVarRdEn    : in std_logic;
    TrigVarMT      : out std_logic;
    TrigVarDataOut : out lword;
    
    Accept         : out std_logic;
    TrigType       : out byte;
    TrigNum        : out lword
    );

end TrigDec;

architecture Behavioral of TrigDec is

  -- Software trigger
  signal iTrigSoft  : std_logic := '0';
  signal iTrigSoft1 : std_logic := '0';   
  signal trigOnce   : std_logic := '0';
  
  -- Trigger signal out
  signal iAccept   : std_logic := '0';
  signal iTrigNum  : lword := X"00000000";
  signal iTrigType : byte  := X"00";  
  
  -- Trigger configuration
  signal trigEn      : std_logic := '0';
  signal trigThres   : byte      := X"06";
  signal engyThres1  : byte      := X"00";
  signal engyThres2  : byte      := X"00";      
  signal fPrmtThres1 : byte      := X"00";
  signal fPrmtThres2 : byte      := X"00";  
  
  -- FSM varibles
  signal trigCnt     : byte  := X"00";
  signal fPrmpt      : std_logic_vector(24 downto 0);
  signal fPrmpt_lt   : std_logic_vector(24 downto 0);  
  signal fPrmptRmn   : std_logic_vector(24 downto 0);

-- lpm_divide component
  component fPrmptDivide
    port (
      CLOCK    : in  std_logic;
      DENOM    : in  std_logic_vector(24 downto 0);
      NUMER    : in  std_logic_vector(24 downto 0);
      QUOTIENT : out std_logic_vector(24 downto 0);
      REMAIN   : out std_logic_vector(24 downto 0)
    );
  end component;
  
  component trigVar
    port (
      RDCLK   : in std_logic;
      WRCLK   : in std_logic;      
      DATA    : in lword;
      RDREQ   : in std_logic;
      ACLR    : in std_logic;
      WRREQ   : in std_logic;
      RDEMPTY : out std_logic;
      WRFULL  : out std_logic;
      Q       : out lword;
      RDUSEDW : out std_logic_vector(9 downto 0)
    );
  end component;
  
  -- Store trigger varibles in trigVar fifo
  signal trigVarWrEn     : std_logic := '0';
  signal iTrigVarRdEn    : std_logic := '0';
  signal iTrigVarMT      : std_logic := '0';
  signal trigVarFull     : std_logic := '0';
  signal trigVarCnt      : std_logic_vector(9 downto 0) := "00" & X"00";
  signal trigVarDataIn   : lword := X"00000000";
  signal iTrigVarDataOut : lword := X"00000000";  
  signal sum300_48_lt    : std_logic_vector(21 downto 0) := "00" & X"00000";
  signal sum5us_0_lt     : std_logic_vector(24 downto 0) := '0' & X"000000";
  signal sum5us_0_1      : std_logic_vector(24 downto 0) := '0' & X"000000";    
  
-- State Machine variables
  type STATE_TYPE is (IDLE, START_TRIG, TRIGGER, TRIG_WAIT);
  signal State     : STATE_TYPE := IDLE; 
  signal NextState : STATE_TYPE := IDLE;
  
begin  -- Behavioral

  -- Trigger 
  Accept    <= iAccept;
  TrigNum   <= iTrigNum;
  TrigType  <= iTrigType;
  
  -- Enable the trigger
  trigEn <= TrigCR(0);
  -- Low energy threshold to cut noise
  trigThres <= TrigCR(8 downto 1);

  -- Energy Cuts
  engyThres1  <= EnrgyThres1(7 downto 0);
  engyThres2  <= EnrgyThres2(7 downto 0);  
  fPrmtThres1 <= FPrmptThres1(7 downto 0);
  fPrmtThres2 <= FPrmptThres2(7 downto 0);  
  
  -- Trigger information to memory
  iTrigVarRdEn   <= TrigVarRdEn;
  TrigVarMT      <= iTrigVarMT;
  TrigVarDataOut <= iTrigVarDataOut;  
  
  -- purpose: synchronous part of state machine
  -- inputs : Clk, Rst
  SynFSM: process (Clk,Rst,State,trigEn)
  begin
    if rising_edge(Clk) then
      if(Rst='1' or trigEn = '0') then
        State         <= IDLE;
        iAccept       <= '0';        
        trigCnt       <= (others => '0');
        iTrigNum      <= (others => '0');
        trigVarWrEn   <= '0';
        trigVarDataIn <= (others => '0');         
        sum300_48_lt  <= (others => '0');
        sum5us_0_lt   <= (others => '0');
        sum5us_0_1    <= (others => '0');
        fPrmpt_lt     <= (others => '0');
        trigOnce      <= '0';
        iTrigSoft     <= '0';
      else
        -- defaults
        State      <= NextState;
        iAccept    <= '0';
        sum5us_0_1 <= Sum5us_0;

        -- Ensure only one trigger per TrigSoft high
        if(TrigSoft(0) = '1' and trigOnce = '0') then
          trigOnce  <= '1';          
          iTrigSoft <= TrigSoft(0);
        else
          iTrigSoft <= '0';
        end if;
        iTrigSoft1  <= iTrigSoft;
        if(TrigSoft(0) = '0' and trigOnce = '1') then
          trigOnce <= '0';
        end if;
          
        if(State = IDLE) then
          trigCnt      <= (others => '0');
          sum300_48_lt <= (others => '0');
          sum5us_0_lt  <= (others => '0');
          fPrmpt_lt    <= (others => '0');                 
        end if;

        if(State = START_TRIG) then
          sum300_48_lt <= Sum300_48;
          sum5us_0_lt  <= Sum5us_0;
          fPrmpt_lt    <= fPrmpt;       
        end if;
        
        if(State = TRIGGER) then
          iAccept  <= '1';                    
          iTrigNum <= iTrigNum + 1;
          trigCnt  <= trigCnt + 1;
        end if;

        if(State = TRIG_WAIT) then
          trigCnt <= trigCnt + 1;
          if(trigCnt < X"02") then
            iAccept <= '1';
          end if;
        end if;        

-- Store basic trigger varibles on accept, trigNum, trigType then sums
        if(iAccept = '1' and trigCnt = X"01") then
          trigVarWrEn   <= '1';
          -- start at trigger number 0
          trigVarDataIn <= iTrigNum-1;
        elsif(trigCnt = X"02") then
          trigVarWrEn   <= '1';
          trigVarDataIn <= X"000000" & iTrigType;
        elsif(trigCnt = X"03") then
          trigVarWrEn   <= '1';
          trigVarDataIn <= "00" & X"00" & sum300_48_lt;
        elsif(trigCnt = X"04") then
          trigVarWrEn   <= '1';
          trigVarDataIn <= "000" & X"0" & sum5us_0_lt;
        elsif(trigCnt = X"05") then
          trigVarWrEn   <= '1';
          trigVarDataIn <= "000" & X"0" & fPrmpt_lt;          
        else
          trigVarWrEn   <= '0';
          trigVarDataIn <= (others => '0');          
        end if;
        
      end if;
        
    end if;

  end process SynFSM;

  -- purpose: Asynchronous part of state machine
  -- inputs : State
  AsynFSM: process (State, Sum300_48, Sum5us_0, trigCnt, fPrmpt,
                    fPrmtThres1, trigThres, iTrigSoft, engyThres1,
                    sum5us_0_1, engyThres2, fPrmtThres2, iTrigSoft1)
  begin  -- process AsynFSM

    case State is
      
      when IDLE => 
        NextState <= IDLE;
        iTrigType <= X"00";        
        if(Sum300_48 > trigThres) then
          NextState <= START_TRIG;
        elsif(iTrigSoft = '1') then
          NextState <= START_TRIG;                           
        end if;

      when START_TRIG => 
        NextState <= START_TRIG;
        if(sum5us_0_1 > engyThres1 and fPrmpt > fPrmtThres1) then
          NextState <= TRIGGER;
          iTrigType <= X"01";
        elsif(sum5us_0_1 < engyThres2 and fPrmpt > fPrmtThres2) then
          NextState <= TRIGGER;          
          iTrigType <= X"02";
         elsif(iTrigSoft1 = '1') then
           NextState <= TRIGGER;          
           iTrigType <= X"FF";            
        end if;

      when TRIGGER =>
        NextState <= TRIG_WAIT;

      when TRIG_WAIT =>
        NextState <= TRIG_WAIT;
        if(trigCnt = X"FA") then
          NextState <= IDLE;
        end if;                
        
      when others => null;
    end case;
    
  end process AsynFSM;

  -- Do fPrmpt divide
  FPrmptDivider : fPrmptDivide
    port map (
      CLOCK               => CLK,
      NUMER               => Sum5us_0,
      DENOM(21 downto 0)  => Sum300_48,
      DENOM(24 downto 22) => "000",      
      QUOTIENT            => fPrmpt,
      REMAIN              => fPrmptRmn
      );
  
  -- store trigger variables
  TrigVarfifo : trigVar
    port map (
      RDCLK   => Clk,
      WRCLK   => Clk,      
      DATA    => trigVarDataIn,
      RDREQ   => iTrigVarRdEn,
      ACLR    => Rst,
      WRREQ   => trigVarWrEn,
      RDEmpty => iTrigVarMT,
      WRFull  => trigVarFull,
      Q       => iTrigVarDataOut,
      RDUSEDW => trigVarCnt
      );
  
end Behavioral;
