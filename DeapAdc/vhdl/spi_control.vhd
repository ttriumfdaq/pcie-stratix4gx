-------------------------------------------------------------------------------
-- SPI Control
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_control is

	port 
	(
		CLK		: in std_logic;
		RESET	: in std_logic;
		
		write_start	: in std_logic;
		read_start	: in std_logic;
		
		DATA_IN		: in std_logic_vector(15 downto 0);
		
		
		
		ready		: in std_logic;
		
		ADDRESS		: out std_logic_vector(7 downto 0);
		DATA		: out std_logic_vector(7 downto 0);
		
		CMD_START	:out std_logic

	);

end entity;

architecture rtl of spi_control is

	-- Build an enumerated type for the state machine
	type state_type is (IDLE, INC_ADD, readSTART, WAIT_1, WAIT_2, WAITBYTE, writeSTART, writeWAIT);
	signal state		: state_type;
	signal next_state   : state_type;


	constant NUM_STAGES : natural := 16;
	constant INST_WIDTH : natural := 8;
	constant DATA_WIDTH : natural := 8;
	
	-- Build an array type for the shift register
	type sr_length is array ((NUM_STAGES-1) downto 0) of std_logic;

	-- Declare the shift register signal
	signal sr: std_logic_vector(NUM_STAGES-1 downto 0);



begin
	
	
	process (CLK, RESET)
		variable addresscount : natural;
		
	begin

		if (RESET = '1') then
	
			state <= IDLE;
		
		elsif (rising_edge(CLK)) then

			case state is
			when IDLE=>
				CMD_START <= '0';
				addresscount := 1;
				if (read_start = '1' and ready = '1') then
					state <= INC_ADD;
				elsif (write_start = '1' and ready = '1') then
					state <= writeSTART;
				end if;
			
			when INC_ADD=>
				CMD_START <= '0';
				addresscount := addresscount + 2;
				state <= readSTART;
								
			when readSTART =>
				CMD_START <= '1';
				ADDRESS	<= std_logic_vector(to_unsigned(addresscount,8));
				state <= WAIT_1;
			
			when WAIT_1=>
				state <= WAIT_2;
			
			when WAIT_2=>
				state <= WAITBYTE;				
						
			when WAITBYTE=>
				CMD_START <= '0';
				if addresscount >= 30 then
					state <= IDLE;
				elsif ready = '1' then
					state <= INC_ADD;
				end if;
			
			when writeSTART=>
				ADDRESS	<= DATA_IN(15 downto 8);
				DATA <= DATA_IN(7 downto 0);
				CMD_START <= '1';
				state <= writeWAIT;
			
			when writeWAIT=>
				state <= IDLE;	

			end case;

		end if;
	end process;

end rtl;





