-------------------------------------------------------------------------------
-- Wrapper for deapTrig simulation files
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
use WORK.deapTrigConst.all;

-- deapTrip port declarations
entity deapTrigSim is
end;

architecture Behavioral of deapTrigSim is

  signal Clk50       : std_logic := '0';
  signal WClk50      : std_logic := '0';  
  signal BClk300     : std_logic := '0';
  signal ClkSlow     : std_logic := '0';
  signal Reset       : std_logic := '1';
  signal iLocked     : std_logic := '0';
  signal iAccept     : std_logic;
  signal fakeDataEn  : std_logic := '0';
  signal firstDone   : std_logic := '0';
  signal firstDone50 : std_logic := '0';
  signal cnt         : std_logic_vector(3 downto 0) := (others => '0');
  signal iDataIn     : std_logic_vector(DataWidth-1 downto 0)   := (others => '0');

  signal iRegRead    : std_logic := '0';
  signal iRegWrite   : std_logic := '0';
  signal setCnt      : std_logic_vector(3 downto 0) := X"0";
  signal iAddr       : lword := X"00000000";
  signal iData       : lword := X"00000000";  
  
begin  -- Behavioral

  deapTrig : entity WORK.deapTrig port map (

    -- Clock Pins    
    CLKIN            => Clk50,
    -- Clock Lock
    LOCKED           => iLocked,
    
    -- Evaluation board LED pins
    USR_LED          => open,
   
    -- Evaluation board reset
    CPU_RESETN       => Reset,
    -- USER Push button (S3) use as chip only reset
    USER_PB1         => '1',
    USER_PB2         => '1',
   
    -- ADC interface signals, warning on schematics ADCs numbered 1-3
    -- Word Framing clock given by ADC     
    WCLK_1            => WClk50,
    WClk_2            => WClk50,          
    WCLK_3            => WClk50,          
    -- Serial Bit clock given by ADC        
    BCLK_1            => BClk300,          
    BCLK_2            => BClk300,          
    BCLK_3            => BClk300,          
    ADC_D_IN          => iDataIn,            
    ADC_CLK           => open,         
    RSTn_1            => open,          
    SLEEP_1           => open,         
    RSTn_2            => open,          
    SLEEP_2           => open,         
    RSTn_3            => open,          
    SLEEP_3           => open,         
    
    -- Port assignments to higher level DeapADC.bdf
    ParallData2PCIeEn => open,
    ParallData2PCIe   => open,

    -- Issue deapTrigReg read an writes
    RegRead           => iRegRead,
    RegWrite          => iRegWrite,
    Addr              => iAddr,
    Data              => iData
   
    );

-- Model Sim generate fake signals
-- Not sure modelsim can handle ps clks 
-- Use 10 ns as clk300 then adjust others accordingly
  
  -- Slow Clk 500 ns
  clockSlow : process
  begin
    wait for 250 ns;
    ClkSlow <= not ClkSlow;
  end process clockSlow;
  
  -- Clk 50
  clock50 : process
  begin
    if(firstDone50 = '0') then
      firstDone50 <= '1';
      wait for 38 ns;
      Clk50 <= not Clk50;
    else
      wait for 60 ns;
      Clk50 <= not Clk50;
    end if;
  end process clock50;

  stimulus : process
  begin
    wait for 5 ns; Reset  <= '0';
    wait for 31 ns; Reset <= '1';
    wait for 5 ns;  iLocked <= '1';
    wait;
  end process stimulus;

-- Issue configuration commands via deapTrigReg
-- purpose: Read/Write to deapTrigReg
  I_0: process (ClkSlow, Reset, setCnt)
  begin  -- process I_0
    if(rising_edge(ClkSlow)) then
      if(Reset = '0') then
        fakeDataEn <= '0';
        iRegRead   <= '0';
        iRegWrite  <= '0';
        setCnt     <= (others => '0');
        iAddr      <= (others => '0');
        iData      <= (others => '0');
      else
-- Try setting enables (InputCR(0) and TrigCR(0)) on
        -- InputCR address 0x1
        if(setCnt = 0) then
          iRegWrite <= '1';          
          iAddr     <= X"00000001";
          iData     <= X"00000001";
          setCnt    <= setCnt + 1;
        elsif(setCnt = 1) then
          iRegWrite <= '1';                    
          iAddr     <= X"00000002";
          iData     <= X"00000001";
          setCnt    <= setCnt + 1;
        elsif(setCnt = 2) then
          fakeDataEn <= '1';
          setCnt     <= setCnt + 1;          
        else
          iRegWrite <= '0';                    
          iAddr     <= X"00000000";
          iData     <= X"00000000";          
        end if; 
      end if;
    end if;
  end process I_0;
  
-- Read in fake data from text file
  rdFile : entity WORK.rdFile port map (
    BClk300    => BClk300,
    WClk50     => WClk50,
    DataOut    => iDataIn,
    fakeDataEn => fakeDataEn
    );

end Behavioral;
