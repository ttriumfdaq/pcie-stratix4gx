-------------------------------------------------------------------------------
-- Build the event before sending it out to the VME/PCIe
-------------------------------------------------------------------------------
-- Event format

-- Header number of wds = 8
-- X"80 & EvtNum"
-- X"RunNum"
-- X"TimeStamp"
-- X"TrigType"
-- X"sumLong"
-- X"sumShort"
-- X"fPrmpt"
-- X"Number of Header wds inc"

-- Data Block 5us of data is 250 samples, 251 wds including wd cnt
-- "000" & X"0" & Sum5us
-- "000" & X"0" & Sum5us
-- ..
-- DataBlockWdCnt

-- Trailer two wds including end of evt marker
-- Total wd cnt
-- X"E0" & EvtNum

-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
use WORK.deapTrigConst.all;

-------------------------------------------------------------------------------

entity EvtBuilder is
  port (
    Clk0         : in std_logic;
    Clk1         : in std_logic;    
    Rst          : in std_logic;
    TrigNum      : in lword;
    TrigType     : in byte;
    RunNum       : in byte;
    Sum5us       : in std_logic_vector(24 downto 0);
    Sum300       : in std_logic_vector(21 downto 0);
    FPrmpt       : in std_logic_vector(24 downto 0);
    TimeStmp     : in lword;
    TrigData     : in std_logic_vector(16 downto 0);
    Accept       : in std_logic;
    SinWaveEn    : in std_logic;    
    evtfifoRdReq : in std_logic;
    EvtfifoCnt   : out std_logic_vector(13 downto 0);
    EvtOut       : out std_logic_vector(63 downto 0)
    );
end EvtBuilder;

architecture behavioral of EvtBuilder is    

-- Tmp for sine wave test
  signal iSinWaveEn : std_logic := '0';
  
  -- Evt accepted
  signal iAccept     : std_logic := '0';

  signal headInCnt   : byte := X"00";
  -- Header fifo signals
  signal headfifoMT  : std_logic := '0';
  signal headfifoWE  : std_logic := '0';
  signal headfifoRE  : std_logic := '0';
  signal headfifoFul : std_logic := '0';
  signal headfifoCnt : std_logic_vector(7 downto 0) := (others => '0');
  signal headIn      : lword := (others => '0');
  signal headOut     : lword := (others => '0');  
  
  component headfifo
    port (
      clock   : in  std_logic;
      data    : in  lword;
      rdreq   : in  std_logic;      
      sclr    : in  std_logic;
      wrreq   : in  std_logic;
      empty   : out std_logic;
      full    : out std_logic;            
      q       : out lword;
      usedw   : out std_logic_vector(7 downto 0)
      );
  end component;

  signal dataInCnt   : word := (others => '0');
  signal datafifoMT  : std_logic := '0';
  signal datafifoWE  : std_logic := '0';
  signal datafifoRE  : std_logic := '0';
  signal datafifoFul : std_logic := '0';
  signal datafifoCnt : std_logic_vector(10 downto 0) := (others => '0');
  signal dataIn      : lword := (others => '0');
  signal dataOut     : lword := (others => '0');  
  
  component datafifo
    port (
      clock   : in  std_logic;
      data    : in  lword;
      rdreq   : in  std_logic;      
      sclr    : in  std_logic;
      wrreq   : in  std_logic;
      empty   : out std_logic;
      full    : out std_logic;      
      q       : out lword;
      usedw   : out std_logic_vector(10 downto 0)
    );
  end component;

  signal evtfifoMT  : std_logic := '0';
  signal evtfifoWE  : std_logic := '0';
  signal evtfifoRE  : std_logic := '0';
  signal evtfifoFul : std_logic := '0';
  signal evtIn      : lword := (others => '0');
  signal evtfifoOut : std_logic_vector(63 downto 0) := (others => '0');

  
  component evtfifo
    port (
      aclr    : in  std_logic;
      data    : in  lword;
      rdclk   : in  std_logic;      
      rdreq   : in  std_logic;
      wrclk   : in  std_logic;      
      wrreq   : in  std_logic;
      q       : out std_logic_vector(63 downto 0);
      rdempty : out std_logic;
      wrfull  : out std_logic;
      wrusedw : out std_logic_vector(13 downto 0)
    );
  end component;

  signal headCnt  : nible := X"0";
  signal dataCnt  : word  := X"0000";
  signal trailCnt : nible := X"0";  

-- State Machine variables
  type STATE_TYPE is (IDLE, HEADER, DATA, TRAILER);
  signal State     : STATE_TYPE := IDLE; 
  signal NextState : STATE_TYPE := IDLE;
  
begin

  -- For sine wave test tie iAccept to not full and enabled
  iSinWaveEn <= SinWaveEn;
  iAccept    <= not evtfifoFul and iSinWaveEn;
  
  -- data out
  EvtOut    <= evtfifoOut;
  evtfifoRE <= evtfifoRdReq;
  
  -- Fill headfifo
  -- purpose: Control the data into the headfifo and data fifo
  HeadCtrl: process (Clk0, Rst,  iAccept )
  begin  -- process HeadCtrl
    if rising_edge(Clk0) then
      if(Rst='1') then
        headIn     <= (others => '0');
        headfifoWE <= '1';
        headInCnt  <= (others => '0'); 
        dataIn     <= (others => '0');       
        datafifoWE <= '0';
        dataInCnt  <= (others => '0');        
      else

        -- data fifo
        if(iAccept = '1' and dataInCnt = X"0000" ) then
          dataIn     <= "000" & X"000" & TrigData;
          datafifoWE <= '1';
          dataInCnt  <= dataInCnt + 1;          
        end if;

        if(dataInCnt <  X"FA"  and dataInCnt /= X"0000") then
          dataInCnt  <= dataInCnt + 1;                    
        end if;
          
        if(dataInCnt =  X"FA") then
          datafifoWE <= '0';
          dataInCnt  <= (others => '0');
        end if;
        
        -- head fifo
        headfifoWE <= '0';        
        if(iAccept = '1') then
          headIn     <= X"B0" & TrigNum(23 downto 0);
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;

        if(headInCnt = 1) then
          headIn     <= X"000000" & RunNum;
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;

        if(headInCnt = 2) then
          headIn     <= TimeStmp;
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;        

        if(headInCnt = 3) then
          headIn     <= X"000000" & TrigType;
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;

        if(headInCnt = 4) then
          headIn     <= "000" & X"0" & Sum5us;
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;

        if(headInCnt = 5) then
          headIn     <= "00" & X"00" & Sum300;
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;

        if(headInCnt = 6) then
          headIn     <= "000" & X"0" & FPrmpt;
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;

        if(headInCnt = 7) then
          headIn     <= X"00000008";
          headfifoWE <= '1';
          headInCnt  <= headInCnt + 1;
        end if;                        

        if(headInCnt = 8) then
          headIn     <= X"00000000";
          headfifoWE <= '0';
          headInCnt  <= (others => '0');
        end if;                                

        
      end if;
    end if;           
  end process HeadCtrl;
  
  -- purpose: synchronous part of state machine
  -- inputs : Clk0, Rst
  SynFSM: process (Clk0,Rst,State)
  begin
    if rising_edge(Clk0) then
      if(Rst='1') then
        State    <= IDLE;        
        headCnt  <= (others => '0');
        dataCnt  <= (others => '0');
        trailCnt <= (others => '0');
      else
        -- defaults
        State      <= NextState;
        evtfifoWE  <= '0';                  

        if State = IDLE then
          headCnt  <= (others => '0');
          dataCnt  <= (others => '0');
          trailCnt <= (others => '0');          
        end if;

        headfifoRE <= '0';        
        if State = HEADER then
          headfifoRE <= '1';
          evtIn      <= headOut;
          evtfifoWE  <= headfifoRE;
          headCnt    <= headCnt + 1;
        end if;

        datafifoRE <= '0';                
        if State = DATA then
          datafifoRE <= '1';
          evtIn      <= dataOut;
          evtfifoWE  <= datafifoRE;          
          dataCnt    <= dataCnt + 1;
        end if;

        if State = TRAILER then
          evtIn     <= X"E0" & TrigNum(23 downto 0);
          evtfifoWE <= '1';
          trailCnt  <= trailCnt + 1;
        end if;        
        
      end if;
    end if;      
  end process SynFSM;

  -- purpose: Asynchronous part of state machine
  -- inputs : State
  AsynFSM: process (State, headfifoMT, headCnt, dataCnt, trailCnt)
  begin  -- process AsynFSM

    case State is
      
      when IDLE => 
        NextState <= IDLE;
        if(headfifoMT /= '1') then
          NextState <= HEADER;
        end if;

      when HEADER =>
        NextState <= HEADER;
        if(headCnt = X"8") then
          NextState <= DATA;
        end if;

      when DATA =>
        NextState <= DATA;
        -- X"FA" 250 samples 5us        
        if(dataCnt = X"FA") then
          NextState <= TRAILER;
        end if;

      when TRAILER =>
        NextState <= TRAILER;
        if(trailCnt = X"1") then
          NextState <= IDLE;
        end if;
        
      when others => null;
    end case;
    
  end process AsynFSM;
        
   headerfifo0 : headfifo
     port map (
       clock   => clk0,
       data    => headIn,
       rdreq   => headfifoRE,      
       sclr    => rst,
       wrreq   => headfifoWE,
       empty   => headfifoMT,
       full    => headfifoFul,      
       q       => headOut,
       usedw   => headfifoCnt
       );


     datafifo0 : datafifo
     port map (
       clock   => clk0,
       data    => dataIn,
       rdreq   => datafifoRE,
       sclr    => rst,
       wrreq   => datafifoWE,
       empty   => datafifoMT,
       full    => datafifoFul,
       q       => dataOut,
       usedw   => datafifoCnt
       );

   -- Data out to VME/PCIe
   evtfifo0 : evtfifo
     port map (
       aclr    => rst,
       data    => evtIn,
       rdclk   => clk1,  
       rdreq   => evtfifoRE,
       wrclk   => clk0,
       wrreq   => evtfifoWE,
       q       => evtfifoOut,       
       rdempty => evtfifoMT,
       wrfull  => evtfifoFul,
       wrusedw => EvtfifoCnt
       );

end Behavioral;
