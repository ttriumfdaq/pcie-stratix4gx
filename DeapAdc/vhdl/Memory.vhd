-------------------------------------------------------------------------------
-- Store data before output
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

library std;
use std.textio.all;

use WORK.deapTrigConst.all;

entity Memory is

  port (
    Clk            : in  std_logic;
    Rst            : in  std_logic;
    -- store trigger variables
    TrigVarRdEn    : out std_logic;
    TrigVarMT      : in std_logic;
    TrigVarDataOut : in lword
    
    );
end Memory;

architecture Behavioral of Memory is

  signal iTrigVarRdEn  : std_logic := '0';
  signal iTrigVarRdEn1 : std_logic := '0';  
  signal dataOut       : integer := 0;      

  
begin  -- Behavioral

  TrigVarRdEn   <= iTrigVarRdEn;  
  iTrigVarRdEn  <= not TrigVarMT;
  
  fileWr: process (Clk, Rst)
    variable s       : line;
    variable fstatus : file_open_status;
    file fOut        : text;
  begin  -- process fileWr
    dataOut     <= conv_integer(UNSIGNED(TrigVarDataOut));    
    file_open(fstatus, fOut, "/home/amuir/work/deap/firmware/trunk/deap_vhdl/outtext.dat", write_mode);
    if(rising_edge(Clk)) then
      if(Rst = '1') then

      else
        iTrigVarRdEn1 <= iTrigVarRdEn;        
        
        if(iTrigVarRdEn1 = '1') then
          write(s, dataOut);
          writeline(fOut,s);
        end if;
      end if;
    end if;
  end process fileWr;
  
end Behavioral;
