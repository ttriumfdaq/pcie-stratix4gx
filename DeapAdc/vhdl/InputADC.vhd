--------------------------------------------------------------------------------
--
-- PROJECT: DEAP 3600 Trigger Card
--
-- MODULE: ADC Kit data acquisition R&D / Digital Front-End Mezzanine card 
--
-- ELEMENT: InputADC
--
-- DESCRIPTION: Interface logic for a octal ADC with LVDS outputs, based on T2K
-- adc_interface module, see end of file.
--
-- This component is intended to interface to an ADC type ADC12EU050.
-- These ADCs use a unipolar sampling clock at frequency F and return the
-- following signals in LVDS:
--  - 1 serial data pair per channel operating at (F x 12) Mbps
--  - 1 clock for de-serialization at (F x 6)
--  - 1 echo clock to find data alignment at F.
-- The 12 bits of data are sent in serial mode, using Double Data Rate
-- (i.e. 1 bit on each transition of the de-serialization clock).


--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
use WORK.deapTrigConst.all;

--------------------------------------------------------------------------------

--
-- Interface to 3xOctal-channel 12 bit ADC with serial LVDS outputs
--
entity inputADC is
  port (

    --
    -- Clocks
    --
    CLK0      : in std_logic;            -- Base clock 50 MHz
    --
    -- General control
    --
    RST        : in std_logic;           -- Asynchronous reset
    --
    -- ADC interface
    --
    -- ADC serial data    
    ADCData     : in  adcdatablock;
     -- Word Framing clock given by ADC
    ADCWClk     : in  std_logic_vector(2 downto 0); 
      -- Bit clock given by ADC
    ADCBClk     : in  std_logic_vector(2 downto 0);

    -- Module CR and SR
    InputCR  : in lword;    
    InputSR  : out lword;
    
    -- Data out of Module
    oDataOut      : out parallWd
    
    );
end inputADC;

architecture behavioral of inputADC is

  component demux
    port (
      aclr      : in  STD_LOGIC := '0';
      datain    : in  STD_LOGIC;
      inclock   : in  STD_LOGIC;
      dataout_h : out STD_LOGIC;
      dataout_l : out STD_LOGIC
      );
  end component;

  component adc_deserial
    port (
      CLOCK   : in  std_logic;
      SCLR    : in  std_logic;
      SHIFTIN : in  std_logic;
      Q       : out std_logic_vector(5 downto 0)
      );
  end component;

  component wclk2sysclk
    port (
      aclr    : in  std_logic;
      data    : in  std_logic_vector(11 downto 0);
      rdclk   : in  std_logic;
      rdreq   : in  std_logic;
      wrclk   : in  std_logic;
      wrreq   : in  std_logic;
      q       : out std_logic_vector(11 downto 0);
      rdempty : out std_logic;
      wrfull  : out std_logic
      );
  end component;

  -- Enable Data taking
  signal dataEn : std_logic := '0';
  
  -- De multiplexed data
  signal data_r  : adcdatablock;
  signal data_f  : adcdatablock;
  signal data_r1 : adcdatablock;
  signal data_f1 : adcdatablock;
  signal data_r2 : adcdatablock;
  signal data_f2 : adcdatablock;
  signal data_r3 : adcdatablock;
  signal data_f3 : adcdatablock;
  signal data_r4 : adcdatablock;
  signal data_f4 : adcdatablock;
  signal data_r5 : adcdatablock;
  signal data_f5 : adcdatablock;         
  -- Parallel data word
  signal dataOut     : parallWd;
  signal parallData  : parallWd;
  
  -- fifo signals
  signal wrreq      : std_logic := '1';
  signal wrfull     : std_logic_vector(23 downto 0);
  signal wclk2rdreq : std_logic;
  signal wclk2rdMT  : std_logic_vector(23 downto 0);                

-- Module status register
  signal iInputSR : lword;
  
-- Keep signal
  attribute preserve : boolean;
  attribute preserve of data_r     : signal is true;
  attribute preserve of data_f     : signal is true;     
  attribute preserve of parallData : signal is true;

begin

  -- Enable data taking
  dataEn   <= InputCR(0);

  -- Parallel data out of module
  oDataOut <= dataOut;
  
  --
  -- DDR input registers for ADC data
  --
--  ADCDEMUX : for j in 2 downto 0 generate
  DEMUX0    : for i in 7 downto 0 generate
    DEMUX1 : demux
      port map (
        ACLR      => RST,
        INCLOCK   => ADCBCLK(0),
        DATAIN    => ADCData(0)(i),
        DATAOUT_H => data_f(0)(i),
        DATAOUT_L => data_r(0)(i)
        );
  end generate;
--  end generate;
  data_f(1)(7 downto 0) <= (others => '0');
  data_r(1)(7 downto 0) <= (others => '0');
  data_f(2)(7 downto 0) <= (others => '0');
  data_r(2)(7 downto 0) <= (others => '0');  
  
  -- For stratix pcie test only use adc0
--   ADCDEMUX : for j in 2 downto 0 generate
--     DEMUX0    : for i in 7 downto 0 generate
--       DEMUX1 : demux
--         port map (
--           ACLR      => RST,
--           INCLOCK   => ADCBCLK(j),
--           DATAIN    => ADCData(j)(i),
--           DATAOUT_H => data_f(j)(i),
--           DATAOUT_L => data_r(j)(i)
--           );
--     end generate;
--   end generate;  
  
  -- Control 1 to 12 shift register enable.  Start once based on WCLK-BCLK relationship
  BCLKCTRLGEN : for j in 2 downto 0 generate  
    BCLKCTRL : process (ADCBCLK(j), rst, dataEn)
    begin
      if rising_edge(ADCBCLK(j)) then  -- rising clock edge      
        if(Rst = '1' or dataEn = '0') then
          data_r5(j) <= (others => '0');
          data_f5(j) <= (others => '0');        
        else
          data_r1(j)        <= data_r(j);
          data_f1(j)        <= data_f(j);
          data_r2(j)        <= data_r1(j);
          data_f2(j)        <= data_f1(j);
          data_r3(j)        <= data_r2(j);
          data_f3(j)        <= data_f2(j);
          data_r4(j)        <= data_r3(j);
          data_f4(j)        <= data_f3(j);
          data_r5(j)        <= data_r4(j);
          data_f5(j)        <= data_f4(j);
        end if;
      end if;
    end process;
  end generate;
  
 -- FIFO to create 12-bit ADC word
 ADCDESALL : for j in 2 downto 0 generate
   ADCDEINI : for i in 7 downto 0 generate
     DESHIFT0 : adc_deserial
       port map (
         CLOCK   => ADCBCLK(j),
         SCLR    => RST,
         SHIFTIN => data_f5(j)(i),         
         Q(0)    => parallData((j*8)+i)(10),
         Q(1)    => parallData((j*8)+i)(8),
         Q(2)    => parallData((j*8)+i)(6),
         
         Q(3)    => parallData((j*8)+i)(4),
         Q(4)    => parallData((j*8)+i)(2),
         Q(5)    => parallData((j*8)+i)(0)         
         );
     DESHIFT1 : adc_deserial
       port map (
         CLOCK   => ADCBCLK(j),
         SCLR    => RST,
         SHIFTIN => data_r5(j)(i),         
         Q(0)    => parallData((j*8)+i)(11),
         Q(1)    => parallData((j*8)+i)(9),
         Q(2)    => parallData((j*8)+i)(7),
         
         Q(3)    => parallData((j*8)+i)(5),
         Q(4)    => parallData((j*8)+i)(3),
         Q(5)    => parallData((j*8)+i)(1)         
         );
   end generate;
 end generate;

  -- Always writing to this fifo so
  wrreq <= '1';
  WClk2SysClkGen : for j in 2 downto 0 generate  
    WClk2SysClkGen_i : for i in 7 downto 0 generate  
      WClk2SysClk0 : wclk2sysclk
        port map (
          aclr    => rst,
          data    => parallData((j*8)+i),
          rdclk   => Clk0,
          rdreq   => wclk2rdreq,
          wrclk   => ADCWCLK(j),
          wrreq   => wrreq,
          q       => dataOut((j*8)+i),
          rdempty => wclk2rdMT((j*8)+i),
          wrfull  => wrfull((j*8)+i)
          );
    end generate;
  end generate;  

  -- WClk2SysClkGen Control
  WClk2SysClk0Ctrl: process (Clk0, rst)
  begin  -- process WClk2SysClk0Ctrl
    if rising_edge(Clk0) then
      if rst = '1' then
        wclk2rdreq <= '0';
      else
        wclk2rdreq <= '1';        
      end if;
    end if;
  end process WClk2SysClk0Ctrl;

-- Status register assignments
  StatusReg: process (Clk0, Rst)
  begin  -- process Status Reg
    if rising_edge(Clk0) then  -- rising clock edge
      InputSR  <= iInputSR;
      if(Rst = '1') then
        iInputSR  <= (others => '0');
      else
        iInputSR(0)            <= wrfull(0);
        iInputSR(24 downto 1)  <= wclk2rdMT;
        iInputSR(31 downto 25) <= (others => '0');        
      end if;
    end if;
  end process StatusReg;
  
end Behavioral;

--------------------------------------------------------------------------------
--
-- PROJECT: T2K 280m TPC readout electronics
--
-- MODULE: ADC Kit data acquisition R&D / Digital Front-End Mezzanine card
--
-- ELEMENT: adc_interface
--
-- DESCRIPTION: Interface logic for a quad ADC with LVDS outputs
--
-- This component is intended to interface to an ADC type ADS5240
-- (Texas Instruments) or AD9229 (Analog Devices).
-- These ADCs use a unipolar sampling clock at frequency F and return the
-- following signals in LVDS:
-- - 1 serial data pair per channel operating at (F x 12) Mbps
-- - 1 clock for de-serialization at (F x 6)
-- - 1 echo clock to find data alignment at F.
-- The 12 bits of data are sent in serial mode, using Double Data Rate
-- (i.e. 1 bit on each transition of the de-serialization clock).
-- The AD9229 sends the MSB first, while the ADS5240 can be programmed
-- in both LSB or MSB first modes. To work transparently with both
-- devices, the MSB first mode must be selected for the ADS5240.
--
-- The adc_interface component comprises:
-- . a series of DDR frip-flop to capture data in the clock domain
-- of the ADC and perform partial de-serialization: each 12-bit
-- sample is composed of 4-bit chunks at Fx3
-- . a 6 word-deep self-addressing FIFO to cross clock domains
-- . synchronization logic to recover frame alignment
--
-- The internal input interface of this component comprises:
-- . an asynchronous RESET
-- . a local clock CLK_LOC that must be running at Fx3
-- . a reference clock CLK_ADC to drive the ADC; must be running at F
-- . a SYNCH_REQ signal to initiate re-synchronization on the serial stream
--
-- The interface to the external ADC comprises:
-- . an output ADC_CLK to dive the ADC
-- . 4 serial data inputs + 1 framing signal input
-- . 1 fast clock input for the clock supplied by the ADC
--
-- The internal output interface comprises:
-- . a 20-bit wide bus Q split into 5x4-bit wide busses to tranfer
-- 5x12-bit words in 3 consecutive chunks of 4-bit at Fx3
-- . an ALIGNED signal to indicate that valid data is being sent out
-- . a LOS signal to indicate that synchronization is lost
--
-- A non-trivial aspect in this design, is that it is highly desirable that
-- the latency from the input of the ADC to the output of this block is fixed
-- and predictible. This is particularly critical when we want to synchronize
-- the stream from several ADC chips, each being is his own clock domain.
-- To achieve proper synchronization it is necessary that:
-- 1. the 4-MSBs of the 12-bit samples of all ADCs are output by this block
-- on the same clock tick,
-- 2. the index of the sample being output is the same for all channels
-- (i.e. all samples correspond to the same sampling time within the
-- precision of the distribution of the sampling clock to all ADCs).
-- 3. the latency from the analog input to the digital output of this block
-- is fixed, predictible, and does not change when the clock of the ADC
-- is suppressed then re-applied.
-- 4. The time to achieve synchronization should be minimized, but can
-- vary from one ADC to the other, or from one re-synchronization operation
-- to the next.
-- To achieve a constant fixed latency, the idea is to keep (almost)
-- synchronous the read and write pointers when operating. Carefully chosen
-- initial values are used for the read and write pointers. Note that the write
-- pointer is incremented at a frequency that is the double of the frequency of
-- the read pointer (but both are locked by one DLL in the FPGA and the PLL of
-- the ADC, so they do not drift).
-- The FIFO is written on the rising edge of LCLK with the last 4 consecutive
-- bits received from the 4 data inputs and the framing signal ADCLK.
-- The sequence of data written for the framing signal is of period 6.
-- The only possible words are the following:
-- 1111
-- 1100
-- 0000
-- 0000
-- 0011
-- 1111
-- It should be noted that words like 0001 *cannot* happen in normal conditions.
-- Note that the INIT signal (delivered by the align component) is synchronous
-- to the read clock (which is 3 times the sampling clock of the ADC). Hence
-- after INIT has been moved to the write clock domain by a dual flip-flop
-- metastability filter, there are only 3 possible cases for the transition
-- of INIT with respect to the sampling clock of the ADC.
-- As a consequence, the 6 possible words of the framing sequence can only
-- be written into the FIFO as follows (assuming that the initial write address
-- is programmed to 0x1):
-- FIFO Address case 1 case 2 case 3
-- 0x1 1111 0000 0011
-- 0x2 1100 0000 1111
-- 0x4 0000 0011 1111
-- 0x8 0000 1111 1100
-- 0x10 0011 1111 0000
-- 0x20 1111 1100 0000
-- This is not completely correct in fact, because this assumes that the
-- propagation delay to the ADC and back is negligible. In reality, this dealy
-- can cause the framing pattern to slip by 2 bits in the FIFO, i.e:
-- FIFO Address case 4 case 5 case 6
-- 0x1 1111 1100 0000
-- 0x2 1111 0000 0011
-- 0x4 1100 0000 1111
-- 0x8 0000 0011 1111
-- 0x10 0000 1111 1100
-- 0x20 0011 1111 0000
--
-- Recall that the read pointer is moved at half speed compared to the write
-- pointer, but moved by 2 positions at a time. It can be seen that:
-- . the synchronization pattern "111111000000" on the read side can only
-- be obtained when reading data from addresses 0x2, 0x8 and 0x20 for
-- case 1, 2 and 3
-- . the content of address 0x1, 0x4 and 0x10 will never lead to proper
-- synchronization for cases 1, 2 and 3.
-- . For case 3, 4 and 5, synchronization can only be achieved by reading
-- data from addresses 0x1, 0x4 and 0x10.
-- . The correct read address sequence shall be:
-- Case 1 : {0x20, 0x2, 0x8}
-- Case 2 : {0x8, 0x20, 0x2}
-- Case 3 : {0x2, 0x8, 0x20}
-- Case 4 : {0x1, 0x4, 0x10}
-- Case 5 : {0x10, 0x1, 0x4}
-- Case 6 : {0x4, 0x10, 0x1}.
-- . for each case, there is one and only one read sequence that leads to
-- synchronization.
-- Note that the alignment exhibit a different latency for the second group
-- of cases. This is not an issue when there is only one ADC to synchronize
-- but it becomes a problem in a multi-ADC configuration because part of
-- them will get synchronize with 1 family of address sequences and others
-- with the other family (depending on trace routing). It is therefore
-- necessary to compensate the latency mismatch between the 2 possible
-- states of synchronization. The signal APOS was added on the framing
-- pattern alignment logic to determine whether we are in case 1,2,3 or
-- 4,5,6. Depending on that, the output of the FIFO and the ALIGNED signal
-- are delayed by 1 or 2 CLK_LOC cycles. The relative delay between the
-- ALIGNED signal for the 2 possible states appears in this case to be a
-- multiple of 3 CLK_LOC cycles (i.e. samples are mutually aligned).
--
-- Because the FIFO cannot hold more than 1 complete 12-bit ADC sample, if
-- synchronization is gained, the index of the ADC sample being output is
-- known. Using a larger FIFO would bring the risk of being synchronized
-- with an unpredictible number of samples queued in the FIFO.
--
--  
--
-- AUTHOR: D. Calvet calvet@hep.saclay.cea.fr
--
-- DATE AND HISTORY:
--  May 2005 : created
--  October 2005: changed FIFO depth from 9-words to 6-words
--  June 2006: added programmable ADC clock invertion through generic statement
--  July 2007: added logic to correct synchronisation mismatch between ADCs
--   in a multi FEC setup. Corrected explanations on synchronization logic:
--   there are 2 kinds of possible final states for the synchronization logic.
--   Both have a different latency and can co-exist in a multi-FEC setup
--   (this is what observed on the real hardware).
--------------------------------------------------------------------------------
