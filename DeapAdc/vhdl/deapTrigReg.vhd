-------------------------------------------------------------------------------
-- Register file for deapTrigReg
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use WORK.deapTrigConst.all;

entity deapTrigReg is

  port (
    Clk          : in std_logic;
    partialRst   : in std_logic;
    rd_reg       : in vmeword;
    InputSR      : in lword;    
    InputCR      : out lword;        
    TrigCR       : out lword;
    VerOut       : out lword;
    SoftRst      : out std_logic;
    RunNum       : out byte;
    FullRst      : out lword;
    SinWaveEn    : out std_logic;    
    TrigSoft     : out lword;
    EnrgyThres1  : out lword;
    EnrgyThres2  : out lword;
    FPrmptThres1 : out lword;
    FPrmptThres2 : out lword 
    );
end deapTrigReg;

architecture Behavioral of deapTrigReg is

  --Firmware version VERSION(X"000") & DATE(X"DDMYY") 
  constant VERSION  : lword := X"0001F50B";
  
  signal TrigCRReg  : lword := X"00000000";
  signal InputCRReg : lword := X"00000000";  
  signal InputSRReg : lword := X"00000000";

begin  -- Behavioral
  
  RegisterMap: process (rd_reg)
  begin  -- process RegisterMap

    VerOut       <= VERSION;
    InputCR      <= rd_reg(1);
    TrigCR       <= rd_reg(2);
    SoftRst      <= rd_reg(3)(0);
    RunNum       <= rd_reg(4)(7 downto 0);
    FullRst      <= rd_reg(5);
    TrigSoft     <= rd_reg(6);
    EnrgyThres1  <= rd_reg(7);
    EnrgyThres2  <= rd_reg(8);    
    FPrmptThres1 <= rd_reg(9);
    FPrmptThres2 <= rd_reg(10);    

    SinWaveEn    <= rd_reg(1)(0);
    
  end process RegisterMap;
  
end Behavioral;
