-------------------------------------------------------------------------------
-- Energy Summation module
-- Inputs:  Number of trigger channels * 12 bits words
--          Clk
--          
-- Outputs: summation short 300ns
--          summation short 5us 
-- Description:
-- Step by step this goes something like: 
-- step0 sum all 24 digitised channels into one word 
-- step1 on consecutive clks shift sum0 until we have five wds containing
-- five consecutive 24 channel summation
-- step2 take sum of the five consecutive wds to make a summation in a
-- 100 ns window
-- step3 on delay 100 ns wd until we have 3 x 100 ns wds covering 300 ns
-- step4 take summation of 3 x 100 ns wds to produce one 300 ns summation wd
-- step5 store 300ns window for use later
-- step6 On each clk add first 20ns wd to 5us wd and remove 50th 20ns wd
-- Will probably have to change 5��s wd production as I'm not sure that
-- step is routable.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use WORK.deapTrigConst.all;

entity EngySum is

  port (
    Clk       : in  std_logic;
    Rst       : in  std_logic;
    DataIn    : in  parallWd;
    TrigCR    : in  lword;
    Sum20_50  : out std_logic_vector(16 downto 0);
    Sum300_0  : out std_logic_vector(21 downto 0);
    Sum300_48 : out std_logic_vector(21 downto 0);    
    Sum5us_0  : out std_logic_vector(24 downto 0)
    );
end EngySum;

architecture Behavioral of EngySum is

  -- Trigger Enable
  signal trigEn : std_logic := '0';
  
  -- 20ns window
  signal sum20_0   : std_logic_vector(16 downto 0) := '0' & X"0000";
  signal sum20_1   : std_logic_vector(16 downto 0) := '0' & X"0000";
  signal sum20_2   : std_logic_vector(16 downto 0) := '0' & X"0000";
  signal sum20_3   : std_logic_vector(16 downto 0) := '0' & X"0000";
  signal sum20_4   : std_logic_vector(16 downto 0) := '0' & X"0000";
  signal iSum20_50 : std_logic_vector(16 downto 0) := '0' & X"0000";      
  
  component sum20
    port (
      CLOCK   : in  std_logic;
      CLKEN   : in  std_logic;
      ACLR    : in  std_logic;
      DATA0X  : in  std_logic_vector(11 downto 0);
      DATA1X  : in  std_logic_vector(11 downto 0);
      DATA2X  : in  std_logic_vector(11 downto 0);
      DATA3X  : in  std_logic_vector(11 downto 0);
      DATA4X  : in  std_logic_vector(11 downto 0);
      DATA5X  : in  std_logic_vector(11 downto 0);
      DATA6X  : in  std_logic_vector(11 downto 0);
      DATA7X  : in  std_logic_vector(11 downto 0);
      DATA8X  : in  std_logic_vector(11 downto 0);
      DATA9X  : in  std_logic_vector(11 downto 0);
      DATA10X : in  std_logic_vector(11 downto 0);
      DATA11X : in  std_logic_vector(11 downto 0);
      DATA12X : in  std_logic_vector(11 downto 0);
      DATA13X : in  std_logic_vector(11 downto 0);
      DATA14X : in  std_logic_vector(11 downto 0);
      DATA15X : in  std_logic_vector(11 downto 0);
      DATA16X : in  std_logic_vector(11 downto 0);
      DATA17X : in  std_logic_vector(11 downto 0);
      DATA18X : in  std_logic_vector(11 downto 0);
      DATA19X : in  std_logic_vector(11 downto 0);
      DATA20X : in  std_logic_vector(11 downto 0);
      DATA21X : in  std_logic_vector(11 downto 0);
      DATA22X : in  std_logic_vector(11 downto 0);
      DATA23X : in  std_logic_vector(11 downto 0);
      RESULT  : out std_logic_vector(16 downto 0)
      );
  end component;

    -- delay 20ns window for 5us
  component delay5000
    port (
      CLOCK    : in  std_logic;
      ACLR     : in  std_logic;
      SHIFTIN  : in  std_logic_vector(16 downto 0);
      SHIFTOUT : out std_logic_vector(16 downto 0);
      TAPS     : out std_logic_vector(16 downto 0)
    );
  end component;

  -- 100ns window
  signal sum100_0  : std_logic_vector(19 downto 0) := X"00000";
  signal sum100_1  : std_logic_vector(19 downto 0) := X"00000";
  signal sum100_2  : std_logic_vector(19 downto 0) := X"00000";  
  
  component sum100
    port (
      CLOCK   : in  std_logic;
      CLKEN   : in  std_logic;
      ACLR    : in  std_logic;      
      DATA0X  : in  std_logic_vector(16 downto 0);
      DATA1X  : in  std_logic_vector(16 downto 0);
      DATA2X  : in  std_logic_vector(16 downto 0);
      DATA3X  : in  std_logic_vector(16 downto 0);
      DATA4X  : in  std_logic_vector(16 downto 0);
      RESULT  : out std_logic_vector(19 downto 0)
      );
  end component;  

  -- delay 100ns windows to create 300ns window
  component delay200
    port (
      CLOCK    : in  std_logic;
      ACLR     : in  std_logic;
      SHIFTIN  : in  std_logic_vector(19 downto 0);
      SHIFTOUT : out std_logic_vector(19 downto 0);
      TAPS0X   : out std_logic_vector(19 downto 0);
      TAPS1X   : out std_logic_vector(19 downto 0)
    );
  end component;

  -- delay 300ns window until 5us window is ready
  component delay4700
    port (
      CLOCK    : in  std_logic;
      ACLR     : in  std_logic;
      SHIFTIN  : in  std_logic_vector(21 downto 0);
      SHIFTOUT : out std_logic_vector(21 downto 0);
      TAPS     : out std_logic_vector(21 downto 0)
    );
  end component;  
  
  -- 300ns window
  signal iSum300_0  : std_logic_vector(21 downto 0) := "00" & X"00000";
  signal iSum300_48 : std_logic_vector(21 downto 0) := "00" & X"00000";  

  
  component sum300
    port (
      CLOCK   : in  std_logic;
      DATA0X  : in  std_logic_vector(19 downto 0);
      DATA1X  : in  std_logic_vector(19 downto 0);
      DATA2X  : in  std_logic_vector(19 downto 0);
      RESULT  : out std_logic_vector(21 downto 0)
      );
  end component;  
  
  -- 5us window
  signal iSum5us_0  : std_logic_vector(24 downto 0) := '0' & X"000000";
  
begin  -- Behavioral

  -- Enable the trigger
  trigEn <= TrigCR(0);

  -- Output summations
  Sum20_50  <= iSum20_50;
  Sum300_0  <= iSum300_0;
  Sum300_48 <= iSum300_48;  
  Sum5us_0  <= iSum5us_0;  
  
  -- purpose: Sum in 20ns window
  Sum20ns : sum20
    port map (
      CLOCK   => Clk,
      CLKEN   => trigEn,
      ACLR    => Rst,
      DATA0X  => DataIn(0),
      DATA1X  => DataIn(1),
      DATA2X  => DataIn(2),
      DATA3X  => DataIn(3),
      DATA4X  => DataIn(4),
      DATA5X  => DataIn(5),
      DATA6X  => DataIn(6),
      DATA7X  => DataIn(7),
      DATA8X  => DataIn(8),
      DATA9X  => DataIn(9),
      DATA10X => DataIn(10),
      DATA11X => DataIn(11),
      DATA12X => DataIn(12),
      DATA13X => DataIn(13),
      DATA14X => DataIn(14),
      DATA15X => DataIn(15),
      DATA16X => DataIn(16),
      DATA17X => DataIn(17),
      DATA18X => DataIn(18),
      DATA19X => DataIn(19),
      DATA20X => DataIn(20),
      DATA21X => DataIn(21),
      DATA22X => DataIn(22),
      DATA23X => DataIn(23),
      RESULT  => sum20_0
      );

  -- Chain together 5 x 20ns windows as input for 100ns window
  chain20: process (Clk, Rst)
  begin  -- process chain20
    if rising_edge(Clk) then  -- rising clock edge
      if Rst = '1' then
        sum20_1 <= (others => '0');
        sum20_2 <= (others => '0');
        sum20_3 <= (others => '0');
        sum20_4 <= (others => '0');         
      else
        sum20_1 <= sum20_0;
        sum20_2 <= sum20_1;
        sum20_3 <= sum20_2;
        sum20_4 <= sum20_3;        
      end if;      
    end if;
  end process chain20;

  -- purpose: Sum in 100ns windows
  Sum100ns : sum100
    port map (
      CLOCK   => Clk,
      CLKEN   => trigEn,
      ACLR    => Rst,      
      DATA0X  => sum20_0,
      DATA1X  => sum20_1,
      DATA2X  => sum20_2,
      DATA3X  => sum20_3,
      DATA4X  => sum20_4,
      RESULT  => sum100_0
      );    

  -- Chain together 3 x 100ns windows as input for 300ns window
  Delay200ns : delay200
    port map (
      CLOCK    => Clk,
      ACLR     => Rst,
      SHIFTIN  => sum100_0,
      TAPS0X   => sum100_1,
      TAPS1X   => sum100_2,
      SHIFTOUT => open
    );
  
  -- purpose: Sum in 300ns windows
  Sum300ns : sum300
    port map (
      CLOCK   => Clk,
      DATA0X  => sum100_0,
      DATA1X  => sum100_1,
      DATA2X  => sum100_2,
      RESULT  => iSum300_0
    );

  -- Delay first 300ns window until 5us ready
  Delay4700ns : delay4700
    port map (
      CLOCK       => Clk,
      ACLR        => Rst,
      SHIFTIN     => iSum300_0,
      TAPS        => iSum300_48,
      SHIFTOUT    => open
    );
  
  -- Delay first 20ns window for 5us
  Delay5000ns : delay5000
    port map (
      CLOCK       => Clk,
      ACLR        => Rst,
      SHIFTIN     => sum20_0,
      TAPS        => iSum20_50,
      SHIFTOUT    => open
    );
  
  -- Generate 5us summation window
  gen5us : process (Clk, Rst)
  begin  -- process
    if rising_edge(Clk) then  -- rising clock edge    
      if Rst = '1' or trigEn = '0' then
        iSum5us_0 <= (others => '0');
      else
        iSum5us_0 <= iSum5us_0 + sum20_0 - iSum20_50;
      end if;          
    end if;
  end process;
  
end Behavioral;
