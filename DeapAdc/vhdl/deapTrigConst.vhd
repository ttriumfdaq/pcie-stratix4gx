-------------------------------------------------------------------------------
-- Constanst file
-- Define supplemental types, subtypes, constansts, and functions
-- Compile in work lib
-- Include: use WORK.deapTrigConst.all in all files.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package deapTrigConst is

-- Register widths  
  constant DataWidth    : integer := 24;
  constant vtDataWidth  : integer := 10;  
  constant EngyTCRWidth : integer := 18;
  constant HSMABusWidth : integer := 16;
  constant TestPatWidth : integer := 12;
  
-- All channels equal zero
  constant ZERO_ALL : std_logic_vector(DataWidth-1 downto 0) := (others => '0');

-- Data types
  subtype nible is std_logic_vector(3 downto 0);  
  subtype byte  is std_logic_vector(7 downto 0);
  subtype word  is std_logic_vector(15 downto 0);
  subtype lword is std_logic_vector(31 downto 0);  
  
-- ADC data in type
  type adcdatablock is array (2 downto 0) of byte;

-- Parallel data word
  type parallWd is array (23 downto 0) of std_logic_vector(11 downto 0);

-- VME Read words
  type vmeword is array (17 downto 0) of std_logic_vector(31 downto 0);
-- Words out to vme/pcie
  type rddataword is array (17 downto 0) of std_logic_vector(31 downto 0);
  
end deapTrigConst;

package body deapTrigConst is

  

end deapTrigConst;
