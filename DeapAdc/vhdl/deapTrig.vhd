-------------------------------------------------------------------------------
-- Top Level file for deapTrig
-------------------------------------------------------------------------------
-- ADC-FMC-HSMA connection names.  Taken from project page
-- https://edev.triumf.ca/projects/edevel00018/documents
--
-- ADC1
-- DOUT(0-7)_p/n - HSMA_RX_D(0-7)_P/N
--
-- ADC2
-- DOUT(8-15)_p/n - HSMA_TX_D(0-7)_P/N
--
-- ADC3
-- DOUT(16-23)_p/n - HSMA_TX_D(15), HSMA_TX_D(16), HSMA_TX_D(13),
-- HSMA_TX_D(12), HSMA_TX_D(14), HSMA_TX4, HSMA_TX3, and
-- HSMA_TX3_P/N 

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
use WORK.deapTrigConst.all;

-- deapTrip port declarations
entity deapTrig is
  port (

    -- Input Clk
    CLKIN  : in std_logic;
    Clk100 : in std_logic;              -- PCIE Clk
    -- Clock Lock
    LOCKED : in std_logic;

    -- Evaluation board LED pins
    USR_LED : out std_logic_vector(3 downto 0);

    -- Evaluation board reset
    CPU_RESETN : in std_logic;
    -- USER Push button (S3) use as chip only reset
    USER_PB1   : in std_logic;
    USER_PB2   : in std_logic;

    -- ADC interface signals, warning on schematics ADCs numbered 1-3
    -- Word Framing clock given by ADC     
    WCLK_1   : in  std_logic;
    WClk_2   : in  std_logic;
    WCLK_3   : in  std_logic;
    -- Serial Bit clock given by ADC        
    BCLK_1   : in  std_logic;
    BCLK_2   : in  std_logic;
    BCLK_3   : in  std_logic;
    ADC_D_IN : in  std_logic_vector(DataWidth-1 downto 0);
    ADC_CLK  : out std_logic;
    RSTn_1   : out std_logic;
    SLEEP_1  : out std_logic;
    RSTn_2   : out std_logic;
    SLEEP_2  : out std_logic;
    RSTn_3   : out std_logic;
    SLEEP_3  : out std_logic;

    -- Connections to PCIe/VME interface
    rd_reg       : in vmeword;
    rd_data_out  : out rddataword;
    -- 64 bit data fifo out 
    DATA_OUT_64b : out std_logic_vector(63 downto 0);
    data_rd_ack  : in std_logic;
    
    RstN         : out std_logic
    
    -- Software I/O ports
--     RegRead           : in std_logic;      
--     RegWrite          : in std_logic;          
--     Addr              : in lword;    
--     Data              : inout lword        

    );
end deapTrig;

architecture Behavioral of deapTrig is

  -- Clock signals
  signal Clk50      : std_logic;
  signal Clk50_bufg : std_logic;
  -- PLL Locked
  signal iLocked     : std_logic;

  -- Resets
  -- Reset     : Hardware reset to ledCtrl and qpll
  -- partialRst: Hardware USER_PB2 reset signals not qpll
  -- softRst   : Software reset, not connected
  -- ADCRst    : ADC reset from USER_PB1
  signal Reset      : std_logic := '0';
  signal partialRst : std_logic := '0';
  signal iSoftRst   : std_logic := '0';
  signal ADCRst     : std_logic := '0';

  -- ADC Data blocks
  signal ADCData   : adcdatablock;
  signal ADC_SEL_n : std_logic;

  -- External com signals 
  signal RAck     : std_logic := '0';      

  -- Trigger signals  
  signal iAccept   : std_logic;
  -- Software trigger
  signal iTrigSoft : lword := (others => '0');
  signal iTrigType : byte  := X"00";
  signal iTrigNum  : lword := X"00000000";
  -- Energy Summation
  signal iSum300_0  : std_logic_vector(21 downto 0) := "00" & X"00000";
  signal iSum300_48 : std_logic_vector(21 downto 0) := "00" & X"00000";  
  signal iSum5us_0  : std_logic_vector(24 downto 0) := '0' & X"000000";  

  -- Trigger variable to store
  signal iTrigVarRdEn    : std_logic := '0';  
  signal iTrigVarMT      : std_logic := '0';      
  signal iTrigVarDataOut : lword := X"00000000";  
  signal iEnrgyThres1    : lword := (others => '0');
  signal iEnrgyThres2    : lword := (others => '0');
  signal iFPrmptThres1   : lword := (others => '0');
  signal iFPrmptThres2   : lword := (others => '0');  
  
  -- Evt block signals
  signal iRunNum   : byte;
  signal iSum20_50 : std_logic_vector(16 downto 0);  
  signal iSum5us   : std_logic_vector(24 downto 0);
  signal iSum300   : std_logic_vector(21 downto 0);
  signal iFPrmpt   : std_logic_vector(24 downto 0);
  signal iTimeStmp : lword := (others => '0');
  signal iEvtOut   : std_logic_vector(63 downto 0) := (others => '0');
  signal EvtfifoCnt : std_logic_vector(13 downto 0) := (others => '0');
  
  -- Control register
  signal InputCR : lword;  
  signal TrigCR  : lword;

-- tmp sine wave test
  signal iSinWaveEn : std_logic := '0';
  
  -- Status register
  signal InputSR : lword;
  
  -- Parallel 12 bit data
  signal                                    ParallData      : parallWd;
-- Keep signal
  attribute preserve                                        : boolean;
  attribute preserve of ParallData                          : signal is true;
  attribute preserve of partialRst                          : signal is true;

begin  -- Behavioral

  -- Clk assignment
  Clk50   <= CLKIN;
  iLocked <= LOCKED;

-- Data out to PCIE
  DATA_OUT_64b <= iEvtOut;
  
-- Tie Board reset to chip reset
  Reset      <= not CPU_RESETN;
  partialRst <= not USER_PB2 or not CPU_RESETN  or not iSoftRst;
  ADCRst     <= not USER_PB1 or not CPU_RESETN;
-- Rst for vme registers
  RstN       <= partialRst;
  
-- Tie ADC interface signals together
  ADC_CLK    <= Clk50;                  -- ADC Sampling Clk
  RSTn_1     <= '0' when ADCRst = '1' else 'Z';
  SLEEP_1    <= not iLocked;             -- ADC sleeps when high
  RSTn_2     <= '0' when ADCRst = '1' else 'Z';
  SLEEP_2    <= not iLocked;             -- ADC sleeps when high
  RSTn_3     <= '0' when ADCRst = '1' else 'Z';
  SLEEP_3    <= not iLocked;             -- ADC sleeps when high  

  ADCData(0) <= ADC_D_IN(7 downto 0);
  -- For Stratix card only able to use first ADC
  -- ADCData(1) <= ADC_D_IN(15 downto 8);
  ADCData(1) <= (others => '0');
  -- ADCData(2) <= ADC_D_IN(23 downto 16);
  ADCData(2) <= (others => '0');  


-- Register Out assignments
  rd_data_out(16) <= "00" & X"0000" & EvtfifoCnt;
  
  
-- Register file for external VME IO
  Reg : entity WORK.deapTrigReg port map (
    Clk          => Clk50,
    partialRst   => partialRst,
    rd_reg       => rd_reg,
    InputSR      => InputSR,    
    TrigCR       => TrigCR,
    InputCR      => InputCR,      
    VerOut       => rd_data_out(0),
    SoftRst      => iSoftRst, 
    RunNum       => iRunNum,
    FullRst      => open,
    SinWaveEn    => iSinWaveEn,    
    TrigSoft     => iTrigSoft, 
    EnrgyThres1  => iEnrgyThres1,
    EnrgyThres2  => iEnrgyThres2,
    FPrmptThres1 => iFPrmptThres1, 
    FPrmptThres2 => iFPrmptThres2 
  );

-- Input Module: Data from ADC
  Input : entity WORK.InputADC port map (
    Clk0       => Clk50,
    Rst        => partialRst,
    ADCData    => ADCData,
    -- ADC word clks
    ADCWClk(0) => WCLK_1,
    ADCWClk(1) => WCLK_2,
    ADCWClk(2) => WCLK_3,
    -- ADC bit clks
    ADCBClk(0) => BCLK_1,
    ADCBClk(1) => BCLK_2,
    ADCBClk(2) => BCLK_3,

    -- Module Control and Status registers
    InputCR       => InputCR,
    InputSR       => InputSR,
    -- parallel data
    oDataOut      => ParallData
    );

-- Memory module: pipeline data used trigger decision
--   Memory : entity work.Memory port map (
--     Clk            => Clk50,
--     Rst            => partialRst,
--     DataIn         => iSum20_50,
--     TrigVarRdEn    => iTrigVarRdEn,  
--     TrigVarMT      => iTrigVarMT,     
--     TrigVarDataOut => iTrigVarDataOut  
--     );

-- Energy summation from slow analog sum
  EngySum : entity WORK.EngySum port map (
    Clk       => Clk50,
    Rst       => partialRst,
    DataIn    => ParallData,
    TrigCR    => TrigCR,
    Sum20_50  => iSum20_50,    
    Sum300_0  => iSum300_0,
    Sum300_48 => iSum300_48,    
    Sum5us_0  => iSum5us_0          
    );

-- Trigger Decision
TrigDec : entity WORK.TrigDec port map (
  Clk            => Clk50,
  Rst            => partialRst,
  Sum300_0       => iSum300_0,
  Sum300_48      => iSum300_48,  
  Sum5us_0       => iSum5us_0,
  TrigCR         => TrigCR,
  EnrgyThres1    => iEnrgyThres1,
  EnrgyThres2    => iEnrgyThres2,
  FPrmptThres1   => iFPrmptThres1,
  FPrmptThres2   => iFPrmptThres2,
  TrigSoft       => iTrigSoft,
  TrigVarRdEn    => iTrigVarRdEn,  
  TrigVarMT      => iTrigVarMT,     
  TrigVarDataOut => iTrigVarDataOut,
  Accept         => iAccept,
  TrigType       => iTrigType,
  TrigNum        => iTrigNum
  );

-- Evt builder
  EvtBuilder : entity WORK.EvtBuilder port map (
    Clk0         => Clk50,
    Clk1         => Clk100,    
    Rst          => partialRst,
    TrigNum      => iTrigNum,
    TrigType     => iTrigType,
    RunNum       => iRunNum,
    Sum5us       => iSum5us,
    Sum300       => iSum300,
    FPrmpt       => iFPrmpt,
    TimeStmp     => iTimeStmp,
    TrigData     => iSum20_50,     
    Accept       => iAccept,
    SinWaveEn    => iSinWaveEn,    
    evtfifoRdReq => data_rd_ack,
    EvtfifoCnt   => EvtfifoCnt,   
    EvtOut       => iEvtOut
    );
  
-- For evaluation board include LED control module
  led : entity WORK.ledCtrl port map (
    Clk0    => Clk50,
    Reset   => Reset,
    locked  => iLocked,
    usr_led => USR_LED
    );

end Behavioral;
