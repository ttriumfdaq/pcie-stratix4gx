delay4700_inst : delay4700 PORT MAP (
		aclr	 => aclr_sig,
		clock	 => clock_sig,
		shiftin	 => shiftin_sig,
		shiftout	 => shiftout_sig,
		taps	 => taps_sig
	);
