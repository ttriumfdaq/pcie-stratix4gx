-------------------------------------------------------------------------------
-- AD9239 ADC SPI Interface
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AD9239_SPI_Interface is

	port 
	(
		CLK		: in std_logic;
		RESET	: in std_logic;

		CMD_START	: in std_logic;
	
		DATA_STROBE	: out std_logic;
		READY		: out std_logic;

		ADDRESS		: in std_logic_vector(7 downto 0);
		DATA_TO_SEND : in std_logic_vector(7 downto 0);
		DATA_RCVD	: out std_logic_vector(7 downto 0);
		wrEN		: out std_logic;
		
		spi_CSB		: out std_logic;
		spi_SCLK	: out std_logic;
		spi_SDO		: out std_logic;
		spi_SDI		: in std_logic
	);

end entity;

architecture rtl of AD9239_SPI_Interface is

	-- Build an enumerated type for the state machine
	type state_type is (IDLE, SEND_INST, SEND_DATA, RECEIVE_DATA);
	signal state		: state_type;
	signal next_state   : state_type;


	constant NUM_STAGES : natural := 16;
	constant INST_WIDTH : natural := 8;
	constant DATA_WIDTH : natural := 8;
	
	-- Build an array type for the shift register
--	type sr_length is array ((NUM_STAGES-1) downto 0) of std_logic;

	-- Declare the shift register signal
	signal sr: std_logic_vector(NUM_STAGES-1 downto 0);


begin

	spi_SCLK <= not CLK;

	process (CLK, RESET)
		variable bitcount	: natural;
	begin
		if (RESET = '1') then
			spi_CSB <= '1';
			state <= IDLE;
			READY <= '0';
		
		elsif (rising_edge(CLK)) then

			case state is
			when IDLE=>
				READY <= '1';
				wrEN <= '0';
				DATA_STROBE <= '0';
				if CMD_START = '1' then
					bitcount := INST_WIDTH;
					sr(bitcount-1 downto 0) <= ADDRESS(7 downto 0);
					state <= SEND_INST;
				else
					DATA_RCVD(7 downto 0) <= sr(DATA_WIDTH-1 downto 0);
					spi_CSB <= '1';
					DATA_STROBE <= '1';
				end if;
				
			when SEND_INST=>--send the register address to the ADC, even addresses are write-only, odd addresses are read-only
				READY <= '0';
				spi_CSB <= '0';	--assert chip select to initiate transfer
				wrEN <= '1'; --enable tristate to output to adc
				-- Set up next bit on SDO and shift data.
				spi_SDO <= sr(INST_WIDTH-1);
				sr(INST_WIDTH-1 downto 1) <= sr((INST_WIDTH-2) downto 0);
				bitcount := bitcount - 1;
				--when done sending address to adc check if it was write or read and goto appropriate state
				if (bitcount = 0 AND ADDRESS(0) = '0') then
					bitcount := DATA_WIDTH;
					sr(bitcount-1 downto 0) <= DATA_TO_SEND;
					state <= SEND_DATA;
				elsif (bitcount = 0 AND ADDRESS(0) = '1') then
					bitcount := DATA_WIDTH + 1; --reading requires one extra clock ??? that's why there are seperate states
					state <= RECEIVE_DATA;
				end if;
			
			
			when SEND_DATA=>
				-- write next bit to SDO
				spi_SDO <= sr(DATA_WIDTH-1);
				sr(DATA_WIDTH-1 downto 1) <= sr((DATA_WIDTH-2) downto 0);
				bitcount := bitcount - 1;
				if (bitcount = 0) then
					state <= IDLE;
				end if;
			
			when RECEIVE_DATA=>
				wrEN <= '0'; -- disable tristate buffer so pin is input
				sr(DATA_WIDTH-1 downto 1) <= sr((DATA_WIDTH-2) downto 0);
				sr(0) <= spi_SDI;
				bitcount := bitcount - 1;
				if bitcount = 0 then
					state <= IDLE;
				end if;

			end case;

		end if;
	end process;

end rtl;





