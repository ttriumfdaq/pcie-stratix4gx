-------------------------------------------------------------------------------
--
-- PROJECT: DEAP 3600 Trigger Card
--
-- MODULE: Assign LED on evaluation card
--
-- ELEMENT: ledCtrl
--
-- DESCRIPTION: Assign LED on evaluation card
--
-- USR_LED(0)   = pll locked
-- USR_LED(1-3) = asset on every 1s

-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
use WORK.deapTrigConst.all;

entity ledCtrl is
  port (
    clk0       : in std_logic;           -- Base clock 50 MHz
    reset      : in std_logic;           -- Asynchronous reset
    locked     : in std_logic;
    usr_led    : out std_logic_vector(3 downto 0)              
    );
end ledCtrl;

architecture behavioral of ledCtrl is

  signal count   : std_logic_vector(25 downto 0) := "00" & X"000000";
  -- LED <= '0' = ON
  signal ledNext : std_logic_vector(2 downto 0)  := "110";  
  signal ledCurr : std_logic_vector(3 downto 0)  := X"0";
  
  begin

-- Assign led vector
    usr_led <= ledCurr;
    
-- purpose: Assign led every 1s
-- type   : sequential
-- inputs : clk0, reset
-- outputs: usr_led(3 downto 0)
    led_assign: process (clk0, reset)
    begin  -- process led_assign
      if(rising_edge(clk0)) then
        if(reset = '1') then
          count               <= (others => '0');
          ledCurr(0)          <= '1';
          ledCurr(3 downto 1) <= "111";
          ledNext             <= "110";
        else
          -- Assign usr_led(0) to pll locked signal
          ledCurr(0) <= not locked;
          -- Rotate other 3 led
          count <= count + 1;
          if(count = X"2FAF080") then
            count               <= (others => '0');
            ledNext             <= ledNext + 1;
            ledCurr(3 downto 1) <= ledNext;
          end if;
        end if;
      end if;
  
    end process led_assign;
    
end Behavioral;
