#!/usr/bin/perl -w

$| = 1;

my $sof = shift @ARGV;

die "Cannot read sof file $sof: $!\n" unless -r $sof;

#my $quartus = "/home/quartus/quartus7.2";
#my $quartus = "/home/olchansk/altera7.2/quartus7.2";
#my $quartus = "/home/olchansk/altera9.0/quartus";
#my $quartus = "/triumfcs/trshare/olchansk/altera/altera9.1/quartus";
my $quartus = "/triumfcs/trshare/olchansk/altera/11.0/quartus";

my $q = `$quartus/bin/quartus_pgm -l`;
print $q;

$q =~ /1\) (.*)\n/m;
my $b = $1;
print "$b\n";

my $cmd = "$quartus/bin/quartus_pgm -c \"$b\" -m JTAG -o \"p;$sof\@1\" ";
print "Running $cmd\n";
system $cmd;

exit 0;
#end
