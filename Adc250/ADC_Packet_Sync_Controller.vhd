---------------------------------------------------------------------
-- ADC Packet Sync Controller
--
-- Chris Ohlmann										July 8, 2010
---------------------------------------------------------------------
-- The controller detects when the transceiver is synchronized
-- through the rx_patterndectect line and ensures that the 32-bit
-- data from the receiver is properly re-aligned into 64-bit packets
-- expected from the ADC.  

-- Note that the 8/16/32-bit sync pattern used to establish the link 
-- must be unique across the 64-bit pattern sent from the ADC and
-- located in the MSB location for proper data alignment.
---------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ADC_Packet_Sync_Controller is

	port(
		clk		 	: in	std_logic;
		reset	 	: in	std_logic;
		datain	 	: in	std_logic_vector(31 downto 0);
		sync_detect	: in	std_logic;
		dataclk		: out	std_logic;
		dataout	 	: out	std_logic_vector(63 downto 0);
		sync_request	: out	std_logic
	);

end entity;

architecture rtl of ADC_Packet_Sync_Controller is

	type state_type is (wait_for_sync, receive_low_word, receive_high_word);
	signal state   : state_type;
	
	signal data_buffer	: std_logic_vector(31 downto 0);

	signal i_sync_request : std_logic;


begin

	sync_request <= i_sync_request;

	process (clk, reset)
	begin
		if reset = '1' then
			state 		<= wait_for_sync;
			i_sync_request	<= '0';
			dataclk		<= '0';
		elsif (rising_edge(clk)) then
			case state is
				when wait_for_sync =>
					dataclk	<= '0';
					if sync_detect = '1' then
						i_sync_request	<= '0';
						state	<= receive_low_word;
						data_buffer	<= datain;
					else
						i_sync_request	<= '1';
					end if;
				
				when receive_low_word =>
					dataclk	<= '0';
					dataout	<= data_buffer & datain;
					state	<= receive_high_word;
				
				when receive_high_word =>
					data_buffer <= datain;
					dataclk	<= '1';
					state	<= receive_low_word;
				
				when others =>
					state <= wait_for_sync;
		
			end case;
		end if;
	end process;

end rtl;