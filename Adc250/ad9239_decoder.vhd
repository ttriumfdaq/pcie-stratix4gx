------------------------------------------------------------
-- Hamming Decoder for Analog Devices ADC, AD9239
-- Author: Bryerton Shaw
-- Date: March 25, 2009
-- Description:
--    Takes the 64-bit data packet from the ADC in
--    parallel and decodes the hamming parity info.
--    The code will correct a one-bit error, and
--    will flag a two-bit error.
--
--    The code is pipelined, and takes 3 clocks
--    to output data.
------------------------------------------------------------
LIBRARY ieee;

USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.numeric_std.ALL;

ENTITY ad9239_decoder IS
    PORT(
        -- Inputs
        rst     : IN    STD_LOGIC;                      -- Asynchronous reset
        clk     : IN    STD_LOGIC;                      -- Clock
        d       : IN    STD_LOGIC_VECTOR(63 DOWNTO 0);  -- Data in, see AD9239 PDF for details, 63 = MSB of Header
        -- Outputs
        d_out   : OUT   STD_LOGIC_VECTOR(55 DOWNTO 0);  -- Data out (data only, no parity)
        e_flag  : OUT   STD_LOGIC;                      -- Error Flag (Two bit error)
        w_flag  : OUT   STD_LOGIC                       -- Warning Flag (One bit error)
    );
END ad9239_decoder;

ARCHITECTURE ad9239_decoder_arch OF ad9239_decoder IS
    SIGNAL err_loc  : INTEGER RANGE 0 TO 64;            -- Location of bit error
    SIGNAL parity   : STD_LOGIC_VECTOR( 6 DOWNTO 0);    -- Calculated Parity
    SIGNAL d_2nd    : STD_LOGIC_VECTOR(55 DOWNTO 0);    -- Intermediate value of 2nd stage data
    SIGNAL d_3rd    : STD_LOGIC_VECTOR(55 DOWNTO 0);    -- Intermediate value of 3rd stage data
    SIGNAL d_mask   : STD_LOGIC_VECTOR(63 DOWNTO 0);    -- XOR mask to fix bit error
    SIGNAL int_e    : STD_LOGIC;                        -- Intermediate error flag value
    SIGNAL int_w    : STD_LOGIC;                        -- Intermediate warning flag value
BEGIN
    decoder: PROCESS(clk,rst)
    BEGIN
        -- Asynchronous Reset
        IF(rst = '1') THEN
            e_flag    <= '0';
            w_flag    <= '0';
            d_out     <= (OTHERS => '0');
--            err_loc   <= 0;
            parity    <= (OTHERS => '0');
            d_2nd     <= (OTHERS => '0');
            d_3rd     <= (OTHERS => '0');
            d_mask    <= (OTHERS => '0');
            int_e     <= '0';
            int_w     <= '0';
         -- Process incoming data on rising edge
         ELSIF(RISING_EDGE(clk)) THEN
            -- Stage 1
            -- Calculate parity of data
            parity(0) <= d(63) XOR           d(61) XOR           d(59) XOR           d(57) XOR           d(55) XOR           d(53) XOR           d(51) XOR           d(49) XOR           d(47) XOR           d(45) XOR           d(43) XOR           d(41) XOR           d(39) XOR           d(37) XOR           d(35) XOR           d(33) XOR d(32) XOR           d(30) XOR           d(28) XOR           d(26) XOR           d(24) XOR           d(22) XOR           d(20) XOR           d(18) XOR d(17) XOR           d(15) XOR           d(13) XOR           d(11) XOR d(10) XOR           d(8) XOR d(0);
            parity(1) <= d(63) XOR d(62) XOR                     d(59) XOR d(58) XOR                     d(55) XOR d(54) XOR                     d(51) XOR d(50) XOR                     d(47) XOR d(46) XOR                     d(43) XOR d(42) XOR                     d(39) XOR d(38) XOR                     d(35) XOR d(34) XOR           d(32) XOR d(31) XOR                     d(28) XOR d(27) XOR                     d(24) XOR d(23) XOR                     d(20) XOR d(19) XOR           d(17) XOR d(16) XOR                     d(13) XOR d(12) XOR           d(10) XOR d(9) XOR d(1);
            parity(2) <= d(63) XOR d(62) XOR d(61) XOR d(60) XOR                                         d(55) XOR d(54) XOR d(53) XOR d(52) XOR                                         d(47) XOR d(46) XOR d(45) XOR d(44) XOR                                         d(39) XOR d(38) XOR d(37) XOR d(36) XOR                               d(32) XOR d(31) XOR d(30) XOR d(29) XOR                                         d(24) XOR d(23) XOR d(22) XOR d(21) XOR                               d(17) XOR d(16) XOR d(15) XOR d(14) XOR                               d(10) XOR d(9)  XOR d(8) XOR d(2);
            parity(3) <= d(63) XOR d(62) XOR d(61) XOR d(60) XOR d(59) XOR d(58) XOR d(57) XOR d(56) XOR                                                                                 d(47) XOR d(46) XOR d(45) XOR d(44) XOR d(43) XOR d(42) XOR d(41) XOR d(40) XOR                                                                       d(32) XOR d(31) XOR d(30) XOR d(29) XOR d(28) XOR d(27) XOR d(26) XOR d(25) XOR                                                                       d(17) XOR d(16) XOR d(15) XOR d(14) XOR d(13) XOR d(12) XOR d(11) XOR d(3);
            parity(4) <= d(63) XOR d(62) XOR d(61) XOR d(60) XOR d(59) XOR d(58) XOR d(57) XOR d(56) XOR d(55) XOR d(54) XOR d(53) XOR d(52) XOR d(51) XOR d(50) XOR d(49) XOR d(48) XOR                                                                                                                                                       d(32) XOR d(31) XOR d(30) XOR d(29) XOR d(28) XOR d(27) XOR d(26) XOR d(25) XOR d(24) XOR d(23) XOR d(22) XOR d(21) XOR d(20) XOR d(19) XOR d(18) XOR d(4);
            parity(5) <= d(63) XOR d(62) XOR d(61) XOR d(60) XOR d(59) XOR d(58) XOR d(57) XOR d(56) XOR d(55) XOR d(54) XOR d(53) XOR d(52) XOR d(51) XOR d(50) XOR d(49) XOR d(48) XOR d(47) XOR d(46) XOR d(45) XOR d(44) XOR d(43) XOR d(42) XOR d(41) XOR d(40) XOR d(39) XOR d(38) XOR d(37) XOR d(36) XOR d(35) XOR d(34) XOR d(33) XOR d(5);
            -- Calculate parity of message (data + parity)
            parity(6) <= d(63) XOR d(62) XOR d(61) XOR d(60) XOR d(59) XOR d(58) XOR d(57) XOR d(56) XOR d(55) XOR d(54) XOR d(53) XOR d(52) XOR d(51) XOR d(50) XOR d(49) XOR d(48) XOR d(47) XOR d(46) XOR d(45) XOR d(44) XOR d(43) XOR d(42) XOR d(41) XOR d(40) XOR d(39) XOR d(38) XOR d(37) XOR d(36) XOR d(35) XOR d(34) XOR d(33) XOR d(32) XOR d(31) XOR d(30) XOR d(29) XOR d(28) XOR d(27) XOR d(26) XOR d(25) XOR d(24) XOR d(23) XOR d(22) XOR d(21) XOR d(20) XOR d(19) XOR d(18) XOR d(17) XOR d(16) XOR d(15) XOR d(14) XOR d(13) XOR d(12) XOR d(11) XOR d(10) XOR d(9) XOR d(8) XOR d(6) XOR d(5) XOR d(4) XOR d(3) XOR d(2) XOR d(1) XOR d(0);
            -- Note d(7) is missing from the parity(6) calculation, as it is unused, and assumed to be zero

            -- Copy input into second-stage buffer
            d_2nd <= d(63 DOWNTO 8);

            -- Stage 2
            -- Copy input into third-stage buffer
            d_3rd <= d_2nd;

            -- Set mask location of one-bit error to one, all others to zero
            -- Note: Even if there is no error, the first parity bit will be flipped by the mask, but this has no affect on the output
            FOR i IN d_mask'RANGE LOOP
                IF i = err_loc THEN
                    d_mask(i) <= '1';
                ELSE
                    d_mask(i) <= '0';
                END IF;
            END LOOP;

            -- Setup warning and error flags
            -- no *apparent* error (parity is valid for all bits)
            IF(parity = "0000000") THEN
                int_e <= '0';
                int_w <= '0';
            -- one bit error has occurred (multiple errors may have occurred in odd amounts (ie: 3, 5, 7, 9, ...))
            ELSIF(parity(6) = '1') THEN
                int_e <= '0';
                int_w <= '1';
            -- two bit error has occurred (message parity is valid, but data parity check failed)
            ELSE
                int_e <= '1';
                int_w <= '0';
            END IF;

            -- Stage 3
            -- Move along pipelined flags
            e_flag <= int_e;
            w_flag <= int_w;

            -- Combine third-stage data with mask and output data, skip power of 2 locations where parity is located
            -- Note: While parity is 'physically' located at the end of the message,
            --       mask is generated by 'placing' parity at power of 2 locations within it.
            --       Since only data is being output, mask locations that fix parity bit locations are skipped.
            d_out(55 DOWNTO 25) <= d_3rd(55 DOWNTO 25) XOR d_mask(63 DOWNTO 33);
            d_out(24 DOWNTO 10) <= d_3rd(24 DOWNTO 10) XOR d_mask(31 DOWNTO 17);
            d_out( 9 DOWNTO  3) <= d_3rd( 9 DOWNTO  3) XOR d_mask(15 DOWNTO  9);
            d_out( 2 DOWNTO  0) <= d_3rd( 2 DOWNTO  0) XOR d_mask( 7 DOWNTO  5);
        END IF;

    END PROCESS decoder;

    -- Define error location
    err_loc <= TO_INTEGER(UNSIGNED(parity(5 DOWNTO 0)));

END ad9239_decoder_arch;
