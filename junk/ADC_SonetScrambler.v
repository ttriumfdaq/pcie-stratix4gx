// $Author: krishnan $
// $Revision: 1.3 $
// $Id: hs_sonet_scr.v,v 1.3 2007/01/23 15:40:27 krishnan Exp $
//`include "hs_memmap_addr.v"
//`include "hs_memmap_def.v"
module hs_sonet_scr (
  I_clk,
  I_resetb,
  I_data,
  I_width,
  O_data
);

//========================================================================
// Interface signals
//========================================================================
//------------------------------------------------------------------------
// Inputs
//------------------------------------------------------------------------
input I_clk;
input I_resetb;
input [63:0] I_data;
input [1:0] I_width;
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Outputs
//------------------------------------------------------------------------
output [63:0] O_data;

reg [63:0] O_data;
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Internal signals
//------------------------------------------------------------------------
wire [63:0] combo_xor_1f;       // SONET XOR logic
//------------------------------------------------------------------------
//========================================================================

//------------------------------------------------------------------------
// SONET scrambler X7 + X6 + 1
//
//    63             8     7     6              1     0                        
//  ----- -------- ----- ----- ----- -------- ----- ----- +++++ +++++
// |     | ...... |     |     |     | ...... |     |     | 6^5 | 5^4 | ...
//  ----- -------- ----- ----- ----- -------- ----- ----- +++++ +++++
//                         |     |                    ^                         
//                         |    ---                   |                        
//                         +---| + |------------------+                        
//                              ---                                            
//------------------------------------------------------------------------
always @ ( posedge I_clk or negedge I_resetb )
  if ( ~I_resetb )
    O_data <= 64'hFE041851E459D4FA;
  else
    O_data <= I_width[1] ? { O_data[15:0], combo_xor_1f[63:16] } :
                           ( I_width[0] ? { O_data[7:0], combo_xor_1f[63:8] } : combo_xor_1f );
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// XOR logic for scrambler
//------------------------------------------------------------------------
assign combo_xor_1f[63] =       O_data[ 6] ^       O_data[ 5] ^ I_data[63] ;
assign combo_xor_1f[62] =       O_data[ 5] ^       O_data[ 4] ^ I_data[62] ;
assign combo_xor_1f[61] =       O_data[ 4] ^       O_data[ 3] ^ I_data[61] ;
assign combo_xor_1f[60] =       O_data[ 3] ^       O_data[ 2] ^ I_data[60] ;
assign combo_xor_1f[59] =       O_data[ 2] ^       O_data[ 1] ^ I_data[59] ;
assign combo_xor_1f[58] =       O_data[ 1] ^       O_data[ 0] ^ I_data[58] ;
assign combo_xor_1f[57] =       O_data[ 0] ^ combo_xor_1f[63] ^ I_data[57] ;
assign combo_xor_1f[56] = combo_xor_1f[63] ^ combo_xor_1f[62] ^ I_data[56] ;
assign combo_xor_1f[55] = combo_xor_1f[62] ^ combo_xor_1f[61] ^ I_data[55] ;
assign combo_xor_1f[54] = combo_xor_1f[61] ^ combo_xor_1f[60] ^ I_data[54] ;
assign combo_xor_1f[53] = combo_xor_1f[60] ^ combo_xor_1f[59] ^ I_data[53] ;
assign combo_xor_1f[52] = combo_xor_1f[59] ^ combo_xor_1f[58] ^ I_data[52] ;
assign combo_xor_1f[51] = combo_xor_1f[58] ^ combo_xor_1f[57] ^ I_data[51] ;
assign combo_xor_1f[50] = combo_xor_1f[57] ^ combo_xor_1f[56] ^ I_data[50] ;
assign combo_xor_1f[49] = combo_xor_1f[56] ^ combo_xor_1f[55] ^ I_data[49] ;
assign combo_xor_1f[48] = combo_xor_1f[55] ^ combo_xor_1f[54] ^ I_data[48] ;
assign combo_xor_1f[47] = combo_xor_1f[54] ^ combo_xor_1f[53] ^ I_data[47] ;
assign combo_xor_1f[46] = combo_xor_1f[53] ^ combo_xor_1f[52] ^ I_data[46] ;
assign combo_xor_1f[45] = combo_xor_1f[52] ^ combo_xor_1f[51] ^ I_data[45] ;
assign combo_xor_1f[44] = combo_xor_1f[51] ^ combo_xor_1f[50] ^ I_data[44] ;
assign combo_xor_1f[43] = combo_xor_1f[50] ^ combo_xor_1f[49] ^ I_data[43] ;
assign combo_xor_1f[42] = combo_xor_1f[49] ^ combo_xor_1f[48] ^ I_data[42] ;
assign combo_xor_1f[41] = combo_xor_1f[48] ^ combo_xor_1f[47] ^ I_data[41] ;
assign combo_xor_1f[40] = combo_xor_1f[47] ^ combo_xor_1f[46] ^ I_data[40] ;
assign combo_xor_1f[39] = combo_xor_1f[46] ^ combo_xor_1f[45] ^ I_data[39] ;
assign combo_xor_1f[38] = combo_xor_1f[45] ^ combo_xor_1f[44] ^ I_data[38] ;
assign combo_xor_1f[37] = combo_xor_1f[44] ^ combo_xor_1f[43] ^ I_data[37] ;
assign combo_xor_1f[36] = combo_xor_1f[43] ^ combo_xor_1f[42] ^ I_data[36] ;
assign combo_xor_1f[35] = combo_xor_1f[42] ^ combo_xor_1f[41] ^ I_data[35] ;
assign combo_xor_1f[34] = combo_xor_1f[41] ^ combo_xor_1f[40] ^ I_data[34] ;
assign combo_xor_1f[33] = combo_xor_1f[40] ^ combo_xor_1f[39] ^ I_data[33] ;
assign combo_xor_1f[32] = combo_xor_1f[39] ^ combo_xor_1f[38] ^ I_data[32] ;
assign combo_xor_1f[31] = combo_xor_1f[38] ^ combo_xor_1f[37] ^ I_data[31] ;
assign combo_xor_1f[30] = combo_xor_1f[37] ^ combo_xor_1f[36] ^ I_data[30] ;
assign combo_xor_1f[29] = combo_xor_1f[36] ^ combo_xor_1f[35] ^ I_data[29] ;
assign combo_xor_1f[28] = combo_xor_1f[35] ^ combo_xor_1f[34] ^ I_data[28] ;
assign combo_xor_1f[27] = combo_xor_1f[34] ^ combo_xor_1f[33] ^ I_data[27] ;
assign combo_xor_1f[26] = combo_xor_1f[33] ^ combo_xor_1f[32] ^ I_data[26] ;
assign combo_xor_1f[25] = combo_xor_1f[32] ^ combo_xor_1f[31] ^ I_data[25] ;
assign combo_xor_1f[24] = combo_xor_1f[31] ^ combo_xor_1f[30] ^ I_data[24] ;
assign combo_xor_1f[23] = combo_xor_1f[30] ^ combo_xor_1f[29] ^ I_data[23] ;
assign combo_xor_1f[22] = combo_xor_1f[29] ^ combo_xor_1f[28] ^ I_data[22] ;
assign combo_xor_1f[21] = combo_xor_1f[28] ^ combo_xor_1f[27] ^ I_data[21] ;
assign combo_xor_1f[20] = combo_xor_1f[27] ^ combo_xor_1f[26] ^ I_data[20] ;
assign combo_xor_1f[19] = combo_xor_1f[26] ^ combo_xor_1f[25] ^ I_data[19] ;
assign combo_xor_1f[18] = combo_xor_1f[25] ^ combo_xor_1f[24] ^ I_data[18] ;
assign combo_xor_1f[17] = combo_xor_1f[24] ^ combo_xor_1f[23] ^ I_data[17] ;
assign combo_xor_1f[16] = combo_xor_1f[23] ^ combo_xor_1f[22] ^ I_data[16] ;
assign combo_xor_1f[15] = combo_xor_1f[22] ^ combo_xor_1f[21] ^ I_data[15] ;
assign combo_xor_1f[14] = combo_xor_1f[21] ^ combo_xor_1f[20] ^ I_data[14] ;
assign combo_xor_1f[13] = combo_xor_1f[20] ^ combo_xor_1f[19] ^ I_data[13] ;
assign combo_xor_1f[12] = combo_xor_1f[19] ^ combo_xor_1f[18] ^ I_data[12] ;
assign combo_xor_1f[11] = combo_xor_1f[18] ^ combo_xor_1f[17] ^ I_data[11] ;
assign combo_xor_1f[10] = combo_xor_1f[17] ^ combo_xor_1f[16] ^ I_data[10] ;
assign combo_xor_1f[ 9] = combo_xor_1f[16] ^ combo_xor_1f[15] ^ I_data[ 9] ;
assign combo_xor_1f[ 8] = combo_xor_1f[15] ^ combo_xor_1f[14] ^ I_data[ 8] ;
assign combo_xor_1f[ 7] = combo_xor_1f[14] ^ combo_xor_1f[13] ^ I_data[ 7] ;
assign combo_xor_1f[ 6] = combo_xor_1f[13] ^ combo_xor_1f[12] ^ I_data[ 6] ;
assign combo_xor_1f[ 5] = combo_xor_1f[12] ^ combo_xor_1f[11] ^ I_data[ 5] ;
assign combo_xor_1f[ 4] = combo_xor_1f[11] ^ combo_xor_1f[10] ^ I_data[ 4] ;
assign combo_xor_1f[ 3] = combo_xor_1f[10] ^ combo_xor_1f[ 9] ^ I_data[ 3] ;
assign combo_xor_1f[ 2] = combo_xor_1f[ 9] ^ combo_xor_1f[ 8] ^ I_data[ 2] ;
assign combo_xor_1f[ 1] = combo_xor_1f[ 8] ^ combo_xor_1f[ 7] ^ I_data[ 1] ;
assign combo_xor_1f[ 0] = combo_xor_1f[ 7] ^ combo_xor_1f[ 6] ^ I_data[ 0] ;
//------------------------------------------------------------------------

endmodule
