////////////////////////////////////////////////////////////////////////////////
// Company: Analog Devices Inc.
// Engineer: MH
// 
// Design Name: AD9239 ADI Link
// Project Name: AD9239 ADI Link
// Target Devices: Altera Stratix II GX
// Tool versions: Quartus II v8.0 sp1
//
// Description: descramble_ethernet64
//
// Dependencies: None
//
// Revision:
// 	1.00 - 10/27/08	- Initial
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module descramble_ethernet64 (par_dclk, idata, o_data); 

input par_dclk;	
input [63:0] idata;
             
output [63:0] o_data;

reg [63:0] prev_i_data;
reg [63:0] i_data, o_data;

always @(posedge par_dclk)
	begin
		i_data <= idata;
		prev_i_data <= i_data;
	end

always @(posedge par_dclk)
	begin
		o_data[63] <= prev_i_data[57]  ^ prev_i_data[38]  ^ i_data[63] ;
		o_data[62] <= prev_i_data[56]  ^ prev_i_data[37]  ^ i_data[62] ;
		o_data[61] <= prev_i_data[55]  ^ prev_i_data[36]  ^ i_data[61] ;
		o_data[60] <= prev_i_data[54]  ^ prev_i_data[35]  ^ i_data[60] ;
		o_data[59] <= prev_i_data[53]  ^ prev_i_data[34]  ^ i_data[59] ;
		o_data[58] <= prev_i_data[52]  ^ prev_i_data[33]  ^ i_data[58] ;
		o_data[57] <= prev_i_data[51]  ^ prev_i_data[32]  ^ i_data[57] ;
		o_data[56] <= prev_i_data[50]  ^ prev_i_data[31]  ^ i_data[56] ;
		o_data[55] <= prev_i_data[49]  ^ prev_i_data[30]  ^ i_data[55] ;
		o_data[54] <= prev_i_data[48]  ^ prev_i_data[29]  ^ i_data[54] ;
		o_data[53] <= prev_i_data[47]  ^ prev_i_data[28]  ^ i_data[53] ;
		o_data[52] <= prev_i_data[46]  ^ prev_i_data[27]  ^ i_data[52] ;
		o_data[51] <= prev_i_data[45]  ^ prev_i_data[26]  ^ i_data[51] ;
		o_data[50] <= prev_i_data[44]  ^ prev_i_data[25]  ^ i_data[50] ;
		o_data[49] <= prev_i_data[43]  ^ prev_i_data[24]  ^ i_data[49] ;
		o_data[48] <= prev_i_data[42]  ^ prev_i_data[23]  ^ i_data[48] ;
		o_data[47] <= prev_i_data[41]  ^ prev_i_data[22]  ^ i_data[47] ;
		o_data[46] <= prev_i_data[40]  ^ prev_i_data[21]  ^ i_data[46] ;
		o_data[45] <= prev_i_data[39]  ^ prev_i_data[20]  ^ i_data[45] ;
		o_data[44] <= prev_i_data[38]  ^ prev_i_data[19]  ^ i_data[44] ;
		o_data[43] <= prev_i_data[37]  ^ prev_i_data[18]  ^ i_data[43] ;
		o_data[42] <= prev_i_data[36]  ^ prev_i_data[17]  ^ i_data[42] ;
		o_data[41] <= prev_i_data[35]  ^ prev_i_data[16]  ^ i_data[41] ;
		o_data[40] <= prev_i_data[34]  ^ prev_i_data[15]  ^ i_data[40] ;
		o_data[39] <= prev_i_data[33]  ^ prev_i_data[14]  ^ i_data[39] ;
		o_data[38] <= prev_i_data[32]  ^ prev_i_data[13]  ^ i_data[38] ;
		o_data[37] <= prev_i_data[31]  ^ prev_i_data[12]  ^ i_data[37] ;
		o_data[36] <= prev_i_data[30]  ^ prev_i_data[11]  ^ i_data[36] ;
		o_data[35] <= prev_i_data[29]  ^ prev_i_data[10]  ^ i_data[35] ;
		o_data[34] <= prev_i_data[28]  ^ prev_i_data[ 9]  ^ i_data[34] ;
		o_data[33] <= prev_i_data[27]  ^ prev_i_data[ 8]  ^ i_data[33] ;
		o_data[32] <= prev_i_data[26]  ^ prev_i_data[ 7]  ^ i_data[32] ;
		o_data[31] <= prev_i_data[25]  ^ prev_i_data[ 6]  ^ i_data[31] ;
		o_data[30] <= prev_i_data[24]  ^ prev_i_data[ 5]  ^ i_data[30] ;
		o_data[29] <= prev_i_data[23]  ^ prev_i_data[ 4]  ^ i_data[29] ;
		o_data[28] <= prev_i_data[22]  ^ prev_i_data[ 3]  ^ i_data[28] ;
		o_data[27] <= prev_i_data[21]  ^ prev_i_data[ 2]  ^ i_data[27] ;
		o_data[26] <= prev_i_data[20]  ^ prev_i_data[ 1]  ^ i_data[26] ;
		o_data[25] <= prev_i_data[19]  ^ prev_i_data[ 0]  ^ i_data[25] ;
		o_data[24] <= prev_i_data[18]  ^ i_data[63] ^ i_data[24] ;
		o_data[23] <= prev_i_data[17]  ^ i_data[62] ^ i_data[23] ;
		o_data[22] <= prev_i_data[16]  ^ i_data[61] ^ i_data[22] ;
		o_data[21] <= prev_i_data[15]  ^ i_data[60] ^ i_data[21] ;
		o_data[20] <= prev_i_data[14]  ^ i_data[59] ^ i_data[20] ;
		o_data[19] <= prev_i_data[13]  ^ i_data[58] ^ i_data[19] ;
		o_data[18] <= prev_i_data[12]  ^ i_data[57] ^ i_data[18] ;
		o_data[17] <= prev_i_data[11]  ^ i_data[56] ^ i_data[17] ;
		o_data[16] <= prev_i_data[10]  ^ i_data[55] ^ i_data[16] ;
		o_data[15] <= prev_i_data[ 9]  ^ i_data[54] ^ i_data[15] ;
		o_data[14] <= prev_i_data[ 8]  ^ i_data[53] ^ i_data[14] ;
		o_data[13] <= prev_i_data[ 7]  ^ i_data[52] ^ i_data[13] ;
		o_data[12] <= prev_i_data[ 6]  ^ i_data[51] ^ i_data[12] ;
		o_data[11] <= prev_i_data[ 5]  ^ i_data[50] ^ i_data[11] ;
		o_data[10] <= prev_i_data[ 4]  ^ i_data[49] ^ i_data[10] ;
		o_data[ 9] <= prev_i_data[ 3]  ^ i_data[48] ^ i_data[ 9] ;
		o_data[ 8] <= prev_i_data[ 2]  ^ i_data[47] ^ i_data[ 8] ;
		o_data[ 7] <= prev_i_data[ 1]  ^ i_data[46] ^ i_data[ 7] ;
		o_data[ 6] <= prev_i_data[ 0]  ^ i_data[45] ^ i_data[ 6] ;
		o_data[ 5] <= i_data[63] ^ i_data[44] ^ i_data[ 5] ;
		o_data[ 4] <= i_data[62] ^ i_data[43] ^ i_data[ 4] ;
		o_data[ 3] <= i_data[61] ^ i_data[42] ^ i_data[ 3] ;
		o_data[ 2] <= i_data[60] ^ i_data[41] ^ i_data[ 2] ;
		o_data[ 1] <= i_data[59] ^ i_data[40] ^ i_data[ 1] ;
		o_data[ 0] <= i_data[58] ^ i_data[39] ^ i_data[ 0] ;
	end           
	
endmodule
