--Legal Notice: (C)2011 Altera Corporation. All rights reserved.  Your
--use of Altera Corporation's design tools, logic functions and other
--software and tools, and its AMPP partner logic functions, and any
--output files any of the foregoing (including device programming or
--simulation files), and any associated documentation or information are
--expressly subject to the terms and conditions of the Altera Program
--License Subscription Agreement or other applicable license agreement,
--including, without limitation, that your use is for the sole purpose
--of programming logic devices manufactured by Altera and sold by Altera
--or its authorized distributors.  Please refer to the applicable
--agreement for further details.


-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

--/** This VHDL file is used for synthesis in chaining DMA design example
--*
--* This file provides the top level for synthesis
--*/
entity PCI_Express_Interface_example_chaining_top is 
        port (
              -- inputs:
                 signal local_rstn_ext : IN STD_LOGIC;
                 signal pcie_rstn : IN STD_LOGIC;
                 signal refclk : IN STD_LOGIC;
                 signal rx_in0 : IN STD_LOGIC;
                 signal rx_in1 : IN STD_LOGIC;
                 signal rx_in2 : IN STD_LOGIC;
                 signal rx_in3 : IN STD_LOGIC;
                 signal rx_in4 : IN STD_LOGIC;
                 signal rx_in5 : IN STD_LOGIC;
                 signal rx_in6 : IN STD_LOGIC;
                 signal rx_in7 : IN STD_LOGIC;
                 signal usr_sw : IN STD_LOGIC_VECTOR (7 DOWNTO 0);

              -- outputs:
                 signal L0_led : OUT STD_LOGIC;
                 signal alive_led : OUT STD_LOGIC;
                 signal comp_led : OUT STD_LOGIC;
                 signal lane_active_led : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal tx_out0 : OUT STD_LOGIC;
                 signal tx_out1 : OUT STD_LOGIC;
                 signal tx_out2 : OUT STD_LOGIC;
                 signal tx_out3 : OUT STD_LOGIC;
                 signal tx_out4 : OUT STD_LOGIC;
                 signal tx_out5 : OUT STD_LOGIC;
                 signal tx_out6 : OUT STD_LOGIC;
                 signal tx_out7 : OUT STD_LOGIC
              );
end entity PCI_Express_Interface_example_chaining_top;


architecture europa of PCI_Express_Interface_example_chaining_top is
  component PCI_Express_Interface_example_chaining_pipen1b is
PORT (
    signal lane_width_code : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        signal tx_out6 : OUT STD_LOGIC;
        signal tx_out4 : OUT STD_LOGIC;
        signal txcompl1_ext : OUT STD_LOGIC;
        signal txelecidle7_ext : OUT STD_LOGIC;
        signal txdatak4_ext : OUT STD_LOGIC;
        signal txelecidle0_ext : OUT STD_LOGIC;
        signal txdatak1_ext : OUT STD_LOGIC;
        signal ref_clk_sel_code : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        signal txelecidle6_ext : OUT STD_LOGIC;
        signal txelecidle2_ext : OUT STD_LOGIC;
        signal txdatak7_ext : OUT STD_LOGIC;
        signal txdatak2_ext : OUT STD_LOGIC;
        signal tx_out0 : OUT STD_LOGIC;
        signal rxpolarity0_ext : OUT STD_LOGIC;
        signal txcompl4_ext : OUT STD_LOGIC;
        signal tx_out2 : OUT STD_LOGIC;
        signal rxpolarity5_ext : OUT STD_LOGIC;
        signal rxpolarity4_ext : OUT STD_LOGIC;
        signal txdata5_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txelecidle3_ext : OUT STD_LOGIC;
        signal txdatak3_ext : OUT STD_LOGIC;
        signal rxpolarity6_ext : OUT STD_LOGIC;
        signal txcompl3_ext : OUT STD_LOGIC;
        signal txelecidle1_ext : OUT STD_LOGIC;
        signal powerdown_ext : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
        signal tx_out3 : OUT STD_LOGIC;
        signal core_clk_out : OUT STD_LOGIC;
        signal tx_out5 : OUT STD_LOGIC;
        signal rxpolarity2_ext : OUT STD_LOGIC;
        signal tx_out7 : OUT STD_LOGIC;
        signal tx_out1 : OUT STD_LOGIC;
        signal txdata6_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txdata0_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxpolarity3_ext : OUT STD_LOGIC;
        signal txcompl2_ext : OUT STD_LOGIC;
        signal phy_sel_code : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        signal rxpolarity1_ext : OUT STD_LOGIC;
        signal txdata1_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txelecidle4_ext : OUT STD_LOGIC;
        signal txdata2_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txdatak6_ext : OUT STD_LOGIC;
        signal txcompl6_ext : OUT STD_LOGIC;
        signal txdata3_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txdatak5_ext : OUT STD_LOGIC;
        signal txcompl7_ext : OUT STD_LOGIC;
        signal txdata7_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txdata4_ext : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal txdatak0_ext : OUT STD_LOGIC;
        signal txdetectrx_ext : OUT STD_LOGIC;
        signal txcompl5_ext : OUT STD_LOGIC;
        signal rxpolarity7_ext : OUT STD_LOGIC;
        signal txcompl0_ext : OUT STD_LOGIC;
        signal test_out_icm : OUT STD_LOGIC_VECTOR (8 DOWNTO 0);
        signal txelecidle5_ext : OUT STD_LOGIC;
        signal rxdata4_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxdatak2_ext : IN STD_LOGIC;
        signal rx_in7 : IN STD_LOGIC;
        signal rxdata5_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxstatus4_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal pipe_mode : IN STD_LOGIC;
        signal rxdatak7_ext : IN STD_LOGIC;
        signal rxstatus3_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal pcie_rstn : IN STD_LOGIC;
        signal rxelecidle7_ext : IN STD_LOGIC;
        signal refclk : IN STD_LOGIC;
        signal rxstatus0_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal rxelecidle0_ext : IN STD_LOGIC;
        signal pld_clk : IN STD_LOGIC;
        signal rxelecidle4_ext : IN STD_LOGIC;
        signal rxvalid4_ext : IN STD_LOGIC;
        signal rxelecidle3_ext : IN STD_LOGIC;
        signal rx_in6 : IN STD_LOGIC;
        signal rx_in1 : IN STD_LOGIC;
        signal rxstatus2_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal rxdata7_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxdatak1_ext : IN STD_LOGIC;
        signal rx_in0 : IN STD_LOGIC;
        signal rxdatak0_ext : IN STD_LOGIC;
        signal rxelecidle1_ext : IN STD_LOGIC;
        signal rxdata1_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rx_in5 : IN STD_LOGIC;
        signal rxelecidle5_ext : IN STD_LOGIC;
        signal rxvalid1_ext : IN STD_LOGIC;
        signal rx_in2 : IN STD_LOGIC;
        signal rxstatus6_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal rxdatak3_ext : IN STD_LOGIC;
        signal rx_in3 : IN STD_LOGIC;
        signal test_in : IN STD_LOGIC_VECTOR (39 DOWNTO 0);
        signal rx_in4 : IN STD_LOGIC;
        signal phystatus_ext : IN STD_LOGIC;
        signal rxdata6_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxdatak4_ext : IN STD_LOGIC;
        signal rxdata3_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxelecidle2_ext : IN STD_LOGIC;
        signal rxdatak5_ext : IN STD_LOGIC;
        signal rxstatus5_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal rxstatus1_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal rxdata0_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxstatus7_ext : IN STD_LOGIC_VECTOR (2 DOWNTO 0);
        signal rxelecidle6_ext : IN STD_LOGIC;
        signal rxvalid3_ext : IN STD_LOGIC;
        signal rxvalid7_ext : IN STD_LOGIC;
        signal rxvalid2_ext : IN STD_LOGIC;
        signal rxvalid6_ext : IN STD_LOGIC;
        signal rxdata2_ext : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
        signal rxvalid5_ext : IN STD_LOGIC;
        signal local_rstn : IN STD_LOGIC;
        signal rxdatak6_ext : IN STD_LOGIC;
        signal rxvalid0_ext : IN STD_LOGIC
      );
  end component PCI_Express_Interface_example_chaining_pipen1b;
                signal alive_cnt :  STD_LOGIC_VECTOR (25 DOWNTO 0);
                signal any_rstn :  STD_LOGIC;
                signal any_rstn_r :  STD_LOGIC;
                signal any_rstn_rr :  STD_LOGIC;
                signal clk_out_buf :  STD_LOGIC;
                signal internal_tx_out0 :  STD_LOGIC;
                signal internal_tx_out1 :  STD_LOGIC;
                signal internal_tx_out2 :  STD_LOGIC;
                signal internal_tx_out3 :  STD_LOGIC;
                signal internal_tx_out4 :  STD_LOGIC;
                signal internal_tx_out5 :  STD_LOGIC;
                signal internal_tx_out6 :  STD_LOGIC;
                signal internal_tx_out7 :  STD_LOGIC;
                signal local_rstn :  STD_LOGIC;
                signal open_lane_width_code :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal open_phy_sel_code :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal open_ref_clk_sel_code :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal phystatus_ext :  STD_LOGIC;
                signal powerdown_ext :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal rxdata0_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata1_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata2_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata3_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata4_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata5_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata6_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdata7_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal rxdatak0_ext :  STD_LOGIC;
                signal rxdatak1_ext :  STD_LOGIC;
                signal rxdatak2_ext :  STD_LOGIC;
                signal rxdatak3_ext :  STD_LOGIC;
                signal rxdatak4_ext :  STD_LOGIC;
                signal rxdatak5_ext :  STD_LOGIC;
                signal rxdatak6_ext :  STD_LOGIC;
                signal rxdatak7_ext :  STD_LOGIC;
                signal rxelecidle0_ext :  STD_LOGIC;
                signal rxelecidle1_ext :  STD_LOGIC;
                signal rxelecidle2_ext :  STD_LOGIC;
                signal rxelecidle3_ext :  STD_LOGIC;
                signal rxelecidle4_ext :  STD_LOGIC;
                signal rxelecidle5_ext :  STD_LOGIC;
                signal rxelecidle6_ext :  STD_LOGIC;
                signal rxelecidle7_ext :  STD_LOGIC;
                signal rxpolarity0_ext :  STD_LOGIC;
                signal rxpolarity1_ext :  STD_LOGIC;
                signal rxpolarity2_ext :  STD_LOGIC;
                signal rxpolarity3_ext :  STD_LOGIC;
                signal rxpolarity4_ext :  STD_LOGIC;
                signal rxpolarity5_ext :  STD_LOGIC;
                signal rxpolarity6_ext :  STD_LOGIC;
                signal rxpolarity7_ext :  STD_LOGIC;
                signal rxstatus0_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus1_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus2_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus3_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus4_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus5_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus6_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxstatus7_ext :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal rxvalid0_ext :  STD_LOGIC;
                signal rxvalid1_ext :  STD_LOGIC;
                signal rxvalid2_ext :  STD_LOGIC;
                signal rxvalid3_ext :  STD_LOGIC;
                signal rxvalid4_ext :  STD_LOGIC;
                signal rxvalid5_ext :  STD_LOGIC;
                signal rxvalid6_ext :  STD_LOGIC;
                signal rxvalid7_ext :  STD_LOGIC;
                signal safe_mode :  STD_LOGIC;
                signal test_in :  STD_LOGIC_VECTOR (39 DOWNTO 0);
                signal test_out_icm :  STD_LOGIC_VECTOR (8 DOWNTO 0);
                signal txcompl0_ext :  STD_LOGIC;
                signal txcompl1_ext :  STD_LOGIC;
                signal txcompl2_ext :  STD_LOGIC;
                signal txcompl3_ext :  STD_LOGIC;
                signal txcompl4_ext :  STD_LOGIC;
                signal txcompl5_ext :  STD_LOGIC;
                signal txcompl6_ext :  STD_LOGIC;
                signal txcompl7_ext :  STD_LOGIC;
                signal txdata0_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata1_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata2_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata3_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata4_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata5_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata6_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdata7_ext :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal txdatak0_ext :  STD_LOGIC;
                signal txdatak1_ext :  STD_LOGIC;
                signal txdatak2_ext :  STD_LOGIC;
                signal txdatak3_ext :  STD_LOGIC;
                signal txdatak4_ext :  STD_LOGIC;
                signal txdatak5_ext :  STD_LOGIC;
                signal txdatak6_ext :  STD_LOGIC;
                signal txdatak7_ext :  STD_LOGIC;
                signal txdetectrx_ext :  STD_LOGIC;
                signal txelecidle0_ext :  STD_LOGIC;
                signal txelecidle1_ext :  STD_LOGIC;
                signal txelecidle2_ext :  STD_LOGIC;
                signal txelecidle3_ext :  STD_LOGIC;
                signal txelecidle4_ext :  STD_LOGIC;
                signal txelecidle5_ext :  STD_LOGIC;
                signal txelecidle6_ext :  STD_LOGIC;
                signal txelecidle7_ext :  STD_LOGIC;
attribute ALTERA_ATTRIBUTE : string;
attribute ALTERA_ATTRIBUTE of any_rstn_r : signal is "SUPPRESS_DA_RULE_INTERNAL=R102";
attribute ALTERA_ATTRIBUTE of any_rstn_rr : signal is "SUPPRESS_DA_RULE_INTERNAL=R102";

begin

  safe_mode <= std_logic'('1');
  local_rstn <= safe_mode OR local_rstn_ext;
  any_rstn <= pcie_rstn AND local_rstn;
  test_in(39 DOWNTO 32) <= std_logic_vector'("00000000");
  test_in(31 DOWNTO 9) <= std_logic_vector'("00000000000000000000000");
  test_in(8 DOWNTO 5) <= A_WE_StdLogicVector((std_logic'(safe_mode) = '1'), std_logic_vector'("0101"), usr_sw(3 DOWNTO 0));
  test_in(4 DOWNTO 0) <= std_logic_vector'("01000");
  --reset Synchronizer
  process (clk_out_buf, any_rstn)
  begin
    if any_rstn = '0' then
      any_rstn_r <= std_logic'('0');
      any_rstn_rr <= std_logic'('0');
    elsif clk_out_buf'event and clk_out_buf = '1' then
      any_rstn_r <= std_logic'('1');
      any_rstn_rr <= any_rstn_r;
    end if;

  end process;

  --LED logic
  process (clk_out_buf, any_rstn_rr)
  begin
    if any_rstn_rr = '0' then
      alive_cnt <= std_logic_vector'("00000000000000000000000000");
      alive_led <= std_logic'('0');
      comp_led <= std_logic'('0');
      L0_led <= std_logic'('0');
      lane_active_led <= std_logic_vector'("0000");
    elsif clk_out_buf'event and clk_out_buf = '1' then
      alive_cnt <= A_EXT (((std_logic_vector'("0000000") & (alive_cnt)) + std_logic_vector'("000000000000000000000000000000001")), 26);
      alive_led <= alive_cnt(25);
      comp_led <= to_std_logic(NOT ((test_out_icm(4 DOWNTO 0) = std_logic_vector'("00011"))));
      L0_led <= to_std_logic(NOT ((test_out_icm(4 DOWNTO 0) = std_logic_vector'("01111"))));
      lane_active_led(3 DOWNTO 0) <= NOT (test_out_icm(8 DOWNTO 5));
    end if;

  end process;

  core : PCI_Express_Interface_example_chaining_pipen1b
    port map(
            core_clk_out => clk_out_buf,
            lane_width_code => open_lane_width_code,
            local_rstn => local_rstn,
            pcie_rstn => pcie_rstn,
            phy_sel_code => open_phy_sel_code,
            phystatus_ext => phystatus_ext,
            pipe_mode => std_logic'('0'),
            pld_clk => clk_out_buf,
            powerdown_ext => powerdown_ext,
            ref_clk_sel_code => open_ref_clk_sel_code,
            refclk => refclk,
            rx_in0 => rx_in0,
            rx_in1 => rx_in1,
            rx_in2 => rx_in2,
            rx_in3 => rx_in3,
            rx_in4 => rx_in4,
            rx_in5 => rx_in5,
            rx_in6 => rx_in6,
            rx_in7 => rx_in7,
            rxdata0_ext => rxdata0_ext,
            rxdata1_ext => rxdata1_ext,
            rxdata2_ext => rxdata2_ext,
            rxdata3_ext => rxdata3_ext,
            rxdata4_ext => rxdata4_ext,
            rxdata5_ext => rxdata5_ext,
            rxdata6_ext => rxdata6_ext,
            rxdata7_ext => rxdata7_ext,
            rxdatak0_ext => rxdatak0_ext,
            rxdatak1_ext => rxdatak1_ext,
            rxdatak2_ext => rxdatak2_ext,
            rxdatak3_ext => rxdatak3_ext,
            rxdatak4_ext => rxdatak4_ext,
            rxdatak5_ext => rxdatak5_ext,
            rxdatak6_ext => rxdatak6_ext,
            rxdatak7_ext => rxdatak7_ext,
            rxelecidle0_ext => rxelecidle0_ext,
            rxelecidle1_ext => rxelecidle1_ext,
            rxelecidle2_ext => rxelecidle2_ext,
            rxelecidle3_ext => rxelecidle3_ext,
            rxelecidle4_ext => rxelecidle4_ext,
            rxelecidle5_ext => rxelecidle5_ext,
            rxelecidle6_ext => rxelecidle6_ext,
            rxelecidle7_ext => rxelecidle7_ext,
            rxpolarity0_ext => rxpolarity0_ext,
            rxpolarity1_ext => rxpolarity1_ext,
            rxpolarity2_ext => rxpolarity2_ext,
            rxpolarity3_ext => rxpolarity3_ext,
            rxpolarity4_ext => rxpolarity4_ext,
            rxpolarity5_ext => rxpolarity5_ext,
            rxpolarity6_ext => rxpolarity6_ext,
            rxpolarity7_ext => rxpolarity7_ext,
            rxstatus0_ext => rxstatus0_ext,
            rxstatus1_ext => rxstatus1_ext,
            rxstatus2_ext => rxstatus2_ext,
            rxstatus3_ext => rxstatus3_ext,
            rxstatus4_ext => rxstatus4_ext,
            rxstatus5_ext => rxstatus5_ext,
            rxstatus6_ext => rxstatus6_ext,
            rxstatus7_ext => rxstatus7_ext,
            rxvalid0_ext => rxvalid0_ext,
            rxvalid1_ext => rxvalid1_ext,
            rxvalid2_ext => rxvalid2_ext,
            rxvalid3_ext => rxvalid3_ext,
            rxvalid4_ext => rxvalid4_ext,
            rxvalid5_ext => rxvalid5_ext,
            rxvalid6_ext => rxvalid6_ext,
            rxvalid7_ext => rxvalid7_ext,
            test_in => test_in,
            test_out_icm => test_out_icm,
            tx_out0 => internal_tx_out0,
            tx_out1 => internal_tx_out1,
            tx_out2 => internal_tx_out2,
            tx_out3 => internal_tx_out3,
            tx_out4 => internal_tx_out4,
            tx_out5 => internal_tx_out5,
            tx_out6 => internal_tx_out6,
            tx_out7 => internal_tx_out7,
            txcompl0_ext => txcompl0_ext,
            txcompl1_ext => txcompl1_ext,
            txcompl2_ext => txcompl2_ext,
            txcompl3_ext => txcompl3_ext,
            txcompl4_ext => txcompl4_ext,
            txcompl5_ext => txcompl5_ext,
            txcompl6_ext => txcompl6_ext,
            txcompl7_ext => txcompl7_ext,
            txdata0_ext => txdata0_ext,
            txdata1_ext => txdata1_ext,
            txdata2_ext => txdata2_ext,
            txdata3_ext => txdata3_ext,
            txdata4_ext => txdata4_ext,
            txdata5_ext => txdata5_ext,
            txdata6_ext => txdata6_ext,
            txdata7_ext => txdata7_ext,
            txdatak0_ext => txdatak0_ext,
            txdatak1_ext => txdatak1_ext,
            txdatak2_ext => txdatak2_ext,
            txdatak3_ext => txdatak3_ext,
            txdatak4_ext => txdatak4_ext,
            txdatak5_ext => txdatak5_ext,
            txdatak6_ext => txdatak6_ext,
            txdatak7_ext => txdatak7_ext,
            txdetectrx_ext => txdetectrx_ext,
            txelecidle0_ext => txelecidle0_ext,
            txelecidle1_ext => txelecidle1_ext,
            txelecidle2_ext => txelecidle2_ext,
            txelecidle3_ext => txelecidle3_ext,
            txelecidle4_ext => txelecidle4_ext,
            txelecidle5_ext => txelecidle5_ext,
            txelecidle6_ext => txelecidle6_ext,
            txelecidle7_ext => txelecidle7_ext
    );

  --vhdl renameroo for output signals
  tx_out0 <= internal_tx_out0;
  --vhdl renameroo for output signals
  tx_out1 <= internal_tx_out1;
  --vhdl renameroo for output signals
  tx_out2 <= internal_tx_out2;
  --vhdl renameroo for output signals
  tx_out3 <= internal_tx_out3;
  --vhdl renameroo for output signals
  tx_out4 <= internal_tx_out4;
  --vhdl renameroo for output signals
  tx_out5 <= internal_tx_out5;
  --vhdl renameroo for output signals
  tx_out6 <= internal_tx_out6;
  --vhdl renameroo for output signals
  tx_out7 <= internal_tx_out7;

end europa;

