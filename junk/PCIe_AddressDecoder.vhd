library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PCIe_AddressDecoder is

	port 
	(
		address		: in std_logic_vector(63 downto 0);
		
		mux_sel 	: out std_logic_vector(1 downto 0);
		pcie_trig   : out std_logic;
		adc_sync  : out std_logic
	);

end entity;

architecture rtl of PCIe_AddressDecoder is

begin

	process (address)
	begin
		case address(27 downto 8) is
		when X"00000" =>
			mux_sel <= "00";  -- test memory
			adc_sync <= '0';
			pcie_trig <= '0';
		when X"00001" =>
			mux_sel <= "01";  -- adc fifo occupancy
			adc_sync <= '0';
			pcie_trig <= '0';
		when X"00002" =>
			mux_sel <= "00";
			adc_sync <= '1';
			pcie_trig <= '0';
		when X"00003" =>
			mux_sel <= "00";
			adc_sync <= '0';
			pcie_trig <= '1';
		when others =>
			mux_sel <= "10";  -- adc fifo data
			adc_sync <= '0';
			pcie_trig <= '0';
		end case;
	end process;

end rtl;
