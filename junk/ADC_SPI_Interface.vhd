---------------------------------------------------------------------
-- ADC SPI Interface
--
-- Chris Ohlmann										July 8, 2010
---------------------------------------------------------------------
-- 
---------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ADC_SPI_Interface is

	port(
		clk		 	: in	std_logic;
		reset	 	: in	std_logic;
		datain	 	: in	std_logic_vector(31 downto 0);
		sync_detect	: in	std_logic;
		dataclk		: out	std_logic;
		dataout	 	: out	std_logic_vector(63 downto 0);
		sync_status	: out	std_logic
	);

end entity;

architecture rtl of ADC_SPI_Interface is

	type state_type is (Idle, receive_low_word, receive_high_word);
	signal state   : state_type;
	
	signal data_buffer	: std_logic_vector(31 downto 0);



begin

	process (clk, reset)
	begin
		if reset = '1' then
			state 	<= Idle;

		elsif (rising_edge(clk)) then
			case state is
				when Idle =>
					
				
				when receive_low_word =>
					
				
				when receive_high_word =>
					
				
				when others =>
					state 	<= Idle;
		
			end case;
		end if;
	end process;

end rtl;