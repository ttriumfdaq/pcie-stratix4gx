-----------------------------------------------------------
-- PCI-Express Avalon-ST TLP Packet decoder
-- Valid only for 128-bit Avalon-ST implementations
--
-- Chris Ohlmann						June 2010
-- *******************************************************
--
-- Only MRd and MWr section of decoder have been fully
-- implemented.  Other packet types are identified, but,
-- other relevent header info has not been decoded.
--
-- ** DMA transfers must be 128-bit aligned. **
-----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity PCIe_TLP_Packet_Decoder is

	port
	(
		clk		 : in	std_logic;
		reset	 : in	std_logic;
		
		-- PCIe Interface
		rx_st_ready	 : out	std_logic;
		rx_st_mask	 : out	std_logic;
		
		rx_st_valid	 : in	std_logic;
		rx_st_sop	 : in	std_logic;  -- Start of Packet
		rx_st_eop	 : in	std_logic;  -- End of Packet
		rx_st_empty	 : in	std_logic;  -- Empty Flag
		rx_st_err	 : in	std_logic;  -- Error
		
		rx_st_data	 : in	std_logic_vector(127 downto 0);
		rx_st_bardec : in	std_logic_vector(7 downto 0);
		rx_st_be	 : in	std_logic_vector(15 downto 0);
		
		-- Appication Interface
		bar_select	 : out	std_logic_vector(7 downto 0);
		address		 : out	std_logic_vector(63 downto 0);	
		requesterID	 : out	std_logic_vector(15 downto 0);
		tag			 : out	std_logic_vector(7 downto 0);	-- PCIe request identifier
		TC	 		 : out	std_logic_vector(2 downto 0);	-- Traffic Class associated with request/response
		dplLength	 : out	std_logic_vector(9 downto 0);	-- Size of Data Payload in DW
		attr		 : out	std_logic_vector(1 downto 0);	-- Attributes for response
		
		MRd		 : out	std_logic;
		MRdLk	 : out	std_logic;
		MWr		 : out	std_logic;
		IORd	 : out	std_logic;
		IOWr	 : out	std_logic;
		CfgRd0	 : out	std_logic;
		CfgWr0	 : out	std_logic;
		CfgRd1	 : out	std_logic;
		CfgWr1	 : out	std_logic;
		Msg		 : out	std_logic;
		MsgD	 : out	std_logic;
		Cpl		 : out	std_logic;
		CplD	 : out	std_logic;
		CplLk	 : out	std_logic;
		CplDLk	 : out	std_logic;
		
		SyncError	: out	std_logic;
		
		
		data_in		 : out	std_logic_vector(127 downto 0);
		byte_en		 : out	std_logic_vector(15 downto 0);
		data_ready	 : out	std_logic
		
	);

end entity;

architecture rtl of PCIe_TLP_Packet_Decoder is

	-- Enumerated type for the state machine
	type state_type is (DecodeHeader, ReceiveData);
	signal state : state_type;

	-- TLP Packet Header Format...  (IMPORTANT: alias only valid when header on data bus)
	-- Generic
	alias TLP_Length	: std_logic_vector(9 downto 0)	is 	rx_st_data(9 downto 0);
	alias TLP_Attr 		: std_logic_vector(1 downto 0)	is	rx_st_data(13 downto 12);	-- Attributes Field
	alias TLP_EP 		: std_logic 	is 	rx_st_data(14);		-- Poisoned Data Flag... associated data is invalid
	alias TLP_TD 		: std_logic 	is 	rx_st_data(15);		-- TLP Digest Field Present Flag
	alias TLP_TC 		: std_logic_vector(2 downto 0)	is	rx_st_data(22 downto 20);	-- Traffic Class
	alias TLP_Type 		: std_logic_vector(4 downto 0)	is	rx_st_data(28 downto 24);
	alias TLP_Fmt 		: std_logic_vector(1 downto 0)	is	rx_st_data(30 downto 29);	-- Format (00b=3DW, 01b=4DW, 10b=3DW w/data, 11b=4DW w/data)
	alias TLP_FirstDWBE	: std_logic_vector(3 downto 0)	is	rx_st_data(35 downto 32);	-- Byte Enable for first payload DW
	alias TLP_LastDWBE	: std_logic_vector(3 downto 0)	is	rx_st_data(39 downto 36);	-- Byte Enable for first payload DW


	signal routing_subfield	: std_logic_vector(2 downto 0);

	-- TLP Command Encoding
	constant cmd_width	: integer := 16;
	signal TLP_command	: std_logic_vector(cmd_width-1 downto 0);
	
	constant MRd_bit	: integer := 0;
	constant MRdLk_bit	: integer := 1;
	constant MWr_bit	: integer := 2;
	constant IORd_bit	: integer := 3;
	constant IOWr_bit	: integer := 4;
	constant CfgRd0_bit	: integer := 5;
	constant CfgWr0_bit	: integer := 6;
	constant CfgRd1_bit	: integer := 7;
	constant CfgWr1_bit	: integer := 8;
	constant Msg_bit	: integer := 9;
	constant MsgD_bit	: integer := 10;
	constant Cpl_bit	: integer := 11;
	constant CplD_bit	: integer := 12;
	constant CplLk_bit	: integer := 13;
	constant CplDLk_bit	: integer := 14;
	constant SyncError_bit	: integer := 15;
	
	
	-- internal signals
	signal iaddress		: std_logic_vector(63 downto 0);
	signal idata_ready	: std_logic;


begin


	rx_st_ready <= '1';	-- No throttling of incoming data
	rx_st_mask	<= '0';	-- Always accept non-posted requests
	
	
	MRd		<= TLP_command(MRd_bit);
	MRdLk	<= TLP_command(MRdLk_bit);
	MWr		<= TLP_command(MWr_bit);
	IORd	<= TLP_command(IORd_bit);
	IOWr	<= TLP_command(IOWr_bit);
	CfgRd0	<= TLP_command(CfgRd0_bit);
	CfgWr0	<= TLP_command(CfgWr0_bit);
	CfgRd1	<= TLP_command(CfgRd1_bit);
	CfgWr1	<= TLP_command(CfgWr1_bit);
	Msg		<= TLP_command(Msg_bit);
	MsgD	<= TLP_command(MsgD_bit);
	Cpl		<= TLP_command(Cpl_bit);
	CplD	<= TLP_command(CplD_bit);
	CplLk	<= TLP_command(CplLk_bit);
	CplDLk	<= TLP_command(CplDLk_bit);	
	
	SyncError	<= TLP_command(SyncError_bit);
	
	address		<= iaddress;
	data_ready	<= idata_ready;


	process (clk, reset)
	begin

		if reset = '1' then
			state <= DecodeHeader;
			TLP_command <= (others => '0');
			idata_ready <= '0';

		elsif (rising_edge(clk)) then

			case state is
			when DecodeHeader=>
				if (rx_st_sop = '1') and (rx_st_valid = '1') then
					case TLP_Type is
			
					when "00000" =>	-- Memory Request
						bar_select	<= rx_st_bardec;
						requesterID <= rx_st_data(63 downto 48);
						tag 		<= rx_st_data(47 downto 40);
						TC			<= TLP_TC;
						dplLength	<= TLP_Length;
						attr		<= TLP_Attr;
						
						case TLP_FirstDWBE is	-- Calculate Lower Address Field for Cpl Responses
						when "1000" => iaddress(1 downto 0) <= "11";
						when "1100" => iaddress(1 downto 0) <= "10";
						when "1110" => iaddress(1 downto 0) <= "01";
						when others => iaddress(1 downto 0) <= "00";
						end case;
						
						case TLP_Fmt is
						when "00" =>	-- Memory Read Request (MRd), 3DW, no data
							TLP_command <= 
								(cmd_width-1 downto MRd_bit+1 => '0') & '1' & (MRd_bit-1 downto 0 => '0');
							iaddress(63 downto 32) <= (others => '0');
							iaddress(31 downto 2) <= rx_st_data(95 downto 66);
							
						when "01" =>	-- Memory Read Request (MRd), 4DW, no data
							TLP_command <= 
								(cmd_width-1 downto MRd_bit+1 => '0') & '1' & (MRd_bit-1 downto 0 => '0');
							iaddress(63 downto 32) <= rx_st_data(95 downto 64);
							iaddress(31 downto 2) <= rx_st_data(127 downto 98);
							
						when "10" =>	-- Memory Write Request (MWr), 3DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto MWr_bit+1 => '0') & '1' & (MWr_bit-1 downto 0 => '0');
							iaddress(63 downto 32) <= (others => '0');
							iaddress(31 downto 2) <= rx_st_data(95 downto 66);

							-- Check address(2)... Qword (64-bit) alignment
							if (rx_st_data(66) = '1') then
								-- non-Qword aligned... first DW of data present with header
								-- Check address(3)... Oword (128-bit) alignment and prepare data for write
								case rx_st_data(67) is
								when '1' =>  -- non-Oword (128-bit) aligned
									data_in(127 downto 96) <= rx_st_data(127 downto 96);
									byte_en <= rx_st_be(15 downto 12) & (11 downto 0 => '0');
								when '0' =>  -- Oword (128-bit) aligned
									data_in(63 downto 32) <= rx_st_data(127 downto 96);
									byte_en <= (15 downto 8 => '0') & rx_st_be(15 downto 12) & (3 downto 0 => '0');
								end case;
								idata_ready <= '1';
							else
								-- Qword aligned... no data with header
								idata_ready <= '0';
							end if;

						
						when "11" =>	-- Memory Write Request (MWr), 4DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto MWr_bit+1 => '0') & '1' & (MWr_bit-1 downto 0 => '0');
							iaddress(63 downto 32) <= rx_st_data(95 downto 64);
							iaddress(31 downto 2) <= rx_st_data(127 downto 98);
							idata_ready <= '0';
								
						end case;
								
								
					when "00001" =>	-- Memory Read Lock Request
						TLP_command <= 
							(cmd_width-1 downto MRdLk_bit+1 => '0') & '1' & (MRdLk_bit-1 downto 0 => '0');
						case TLP_Fmt is
						when "10" =>	-- Memory Read Lock Request (MRdLk), 3DW, no data
							
						when others =>	-- Memory Read Lock Request (MRdLk), 4DW, no data
												
						end case;
							
					when "00010" =>	-- IO Request
						bar_select <= rx_st_bardec;
						case TLP_Fmt is
						when "00" =>	-- IO Read Request (IORd), 3DW, no data
							TLP_command <= 
								(cmd_width-1 downto IORd_bit+1 => '0') & '1' & (IORd_bit-1 downto 0 => '0');
						when others =>	-- IO Write Request (IOWr), 3DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto IOWr_bit+1 => '0') & '1' & (IOWr_bit-1 downto 0 => '0');	
						end case;
							
					when "00100" =>	-- Config Type 0 Request
						case TLP_Fmt is
						when "00" =>	-- Config Type 0 Read Request (CfgRd0), 3DW, no data
							TLP_command <= 
								(cmd_width-1 downto CfgRd0_bit+1 => '0') & '1' & (CfgRd0_bit-1 downto 0 => '0');
						when others =>	-- Config Type 0 Write Request (CfgWr0), 3DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto CfgWr0_bit+1 => '0') & '1' & (CfgWr0_bit-1 downto 0 => '0');										
						end case;
							
							
					when "00101" =>	-- Config Type 1 Request (not required for endpoint)
						case TLP_Fmt is
						when "00" =>	-- Config Type 1 Read Request (CfgRd0), 3DW, no data
							TLP_command <= 
								(cmd_width-1 downto CfgRd1_bit+1 => '0') & '1' & (CfgRd1_bit-1 downto 0 => '0');
						when others =>	-- Config Type 1 Write Request (CfgWr0), 3DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto CfgWr1_bit+1 => '0') & '1' & (CfgWr1_bit-1 downto 0 => '0');											
						end case;
							
					when "10000" | "10001" | "10010" | "10011" | "10100" | "10101" | "10110" | "10111" =>	-- Message Request 
						TLP_command <= 
							(cmd_width-1 downto Msg_bit+1 => '0') & '1' & (Msg_bit-1 downto 0 => '0');
						case TLP_Fmt is
						when "01" =>	-- Message Request (Msg), 4DW, no data
							routing_subfield <= TLP_Type(2 downto 0);
										
						when others =>	-- Message Request (Msg), 4DW, w/ data
							routing_subfield <= TLP_Type(2 downto 0);
																								
						end case;
							
					when "01010" =>	-- Completion
						case TLP_Fmt is
						when "00" =>	-- Completion (Cpl), 3DW, no data
							TLP_command <= 
								(cmd_width-1 downto Cpl_bit+1 => '0') & '1' & (Cpl_bit-1 downto 0 => '0');
						when others =>	-- Completion w/Data (CplD), 3DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto CplD_bit+1 => '0') & '1' & (CplD_bit-1 downto 0 => '0');
						end case;
								
					when "01011" =>	-- Completion-Locked
						case TLP_Fmt is
						when "00" =>	-- Completion-Locked (CplLk), 3DW, no data
							TLP_command <= 
								(cmd_width-1 downto CplLk_bit+1 => '0') & '1' & (CplLk_bit-1 downto 0 => '0');
						when others =>	-- Completion-Locked w/Data (CplDLk), 3DW, w/ data
							TLP_command <= 
								(cmd_width-1 downto CplDLk_bit+1 => '0') & '1' & (CplDLk_bit-1 downto 0 => '0');
						end case;
							
							
					when others =>
						TLP_command <= 
							(cmd_width-1 downto SyncError_bit+1 => '0') & '1' & (SyncError_bit-1 downto 0 => '0');
						
					end case;
						
					if (rx_st_eop = '1') then
						state <= DecodeHeader;
					else
						state <= ReceiveData;
					end if;
				else
					TLP_command <= (others => '0');
					state <= DecodeHeader;
				end if;
				
				
				
			when ReceiveData=>
				--TLP_command(cmd_width-1 downto 0) <= (others => '0');
								
				if (rx_st_valid = '1') then
					-- Check address(3)... Oword (128-bit) alignment and prepare data for write
					if (iaddress(3) = '1') then
						-- non-Oword aligned... align data for 128-bit memory interface
						-- only 2 DW's can be valid without being an unsupported unaligned transfer
						data_in(127 downto 64) <= rx_st_data(63 downto 0);
						data_in(63 downto 0) <= rx_st_data(127 downto 64);  
						byte_en <= rx_st_be(7 downto 0) & (7 downto 0 => '0');
					else
						-- Oword aligned... data is aligned
						data_in <= rx_st_data;
						if (rx_st_empty = '1') then
							byte_en <= (15 downto 8 => '0') & rx_st_be(7 downto 0);
						else
							byte_en <= rx_st_be;
						end if;
					end if;

					-- check if previous data was written and increment 128-bit address
					if (idata_ready = '1') then
						iaddress <= std_logic_vector(unsigned(iaddress) + 16);
					end if;
					
					idata_ready <= '1';
			
					if (rx_st_eop = '1') then
						state <= DecodeHeader;
					else
						state <= ReceiveData;
					end if;
				else
					idata_ready <= '0';
				end if;


			end case;

		end if;
	end process;

end rtl;
