-----------------------------------------------------------
-- PCI-Express Avalon-ST TLP Packet Encoder
-----------------------------------------------------------
-- *Valid only for 128-bit Avalon-ST implementations
-- 
-- *Only supports 128-bit aligned DMA transfers!!
--
-- Chris Ohlmann						June 2010
-----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PCIe_TLP_Packet_Encoder is

	port
	(
		clk		 : in	std_logic;
		reset	 : in	std_logic;
		
		-- PCIe Interface
		tx_cred		 : in	std_logic_vector(35 downto 0);
		tx_st_ready	 : in	std_logic;
		
		tx_st_valid	 : out	std_logic;
		tx_st_sop	 : out	std_logic;  -- Start of Packet
		tx_st_eop	 : out	std_logic;  -- End of Packet
		tx_st_empty	 : out	std_logic;  -- Empty Flag
		tx_st_err	 : out	std_logic;  -- Error
		
		tx_st_data	 : out	std_logic_vector(127 downto 0);
		
		
		-- Appication Interface
		address_in	 : in	std_logic_vector(63 downto 0);
		bar_select	 : in	std_logic_vector(7 downto 0);
		requesterID	 : in	std_logic_vector(15 downto 0);
		tag			 : in	std_logic_vector(7 downto 0);	-- PCIe request identifier
		TC	 		 : in	std_logic_vector(2 downto 0);	-- Traffic Class associated with request/response
		dplLength	 : in	std_logic_vector(9 downto 0);	-- Size of Data Payload in DW
		attr		 : in	std_logic_vector(1 downto 0);	-- Attributes for response
		
		rd_address	 : out	std_logic_vector(63 downto 0);
		rd_ack		 : out	std_logic;
		
		MRd		 : in	std_logic;
		MRdLk	 : in	std_logic;
		MWr		 : in	std_logic;
		IORd	 : in	std_logic;
		IOWr	 : in	std_logic;
		CfgRd0	 : in	std_logic;
		CfgWr0	 : in	std_logic;
		CfgRd1	 : in	std_logic;
		CfgWr1	 : in	std_logic;
		Msg		 : in	std_logic;
		MsgD	 : in	std_logic;
		Cpl		 : in	std_logic;
		CplD	 : in	std_logic;
		CplLk	 : in	std_logic;
		CplDLk	 : in	std_logic;
		
		data_to_send : in	std_logic_vector(127 downto 0)
		
	);

end entity;

architecture rtl of PCIe_TLP_Packet_Encoder is

	-- Enumerated type for the state machine
	type state_type is (Idle, CplD_Header, CplD_DataTransfer, MemorySetup2, MemorySetup1);
	signal state : state_type;
	signal next_state : state_type;

	-- tx_cred[35..0] alias definitions...
	alias CplData_creds	: std_logic_vector(11 downto 0)	is 	tx_cred(35 downto 24);
	alias CplHdr_creds	: std_logic_vector(2 downto 0)	is 	tx_cred(23 downto 21);
	alias NPData_creds	: std_logic_vector(2 downto 0)	is 	tx_cred(20 downto 18);
	alias NPHdr_creds	: std_logic_vector(2 downto 0)	is 	tx_cred(17 downto 15);
	alias PData_creds	: std_logic_vector(11 downto 0)	is 	tx_cred(14 downto 3);
	alias PHdr_creds	: std_logic_vector(2 downto 0)	is 	tx_cred(2 downto 0);

	-- TLP Packet Header Format...
	-- Generic fields only	
	alias TLP_Length	: std_logic_vector(9 downto 0)	is 	tx_st_data(9 downto 0);
	alias TLP_Attr 		: std_logic_vector(1 downto 0)	is	tx_st_data(13 downto 12);	-- Attributes Field
	alias TLP_EP 		: std_logic 	is 	tx_st_data(14);		-- Poisoned Data Flag... associated data is invalid
	alias TLP_TD 		: std_logic 	is 	tx_st_data(15);		-- TLP Digest Field Present Flag
	alias TLP_TC 		: std_logic_vector(2 downto 0)	is	tx_st_data(22 downto 20);	-- Traffic Class
	alias TLP_Type 		: std_logic_vector(4 downto 0)	is	tx_st_data(28 downto 24);
	alias TLP_Fmt 		: std_logic_vector(1 downto 0)	is	tx_st_data(30 downto 29);	-- Format (00b=3DW, 01b=4DW, 10b=3DW w/data, 11b=4DW w/data)
	alias TLP_FirstDWBE	: std_logic_vector(3 downto 0)	is	tx_st_data(35 downto 32);	-- Byte Enable for first payload DW
	alias TLP_LastDWBE	: std_logic_vector(3 downto 0)	is	tx_st_data(39 downto 36);	-- Byte Enable for first payload DW

	-- Cpl Header fields
	alias Cpl_ByteCount	: std_logic_vector(11 downto 0)	is	tx_st_data(43 downto 32);	-- Byte Count remaining until request is satisfied
	alias Cpl_BCM 		: std_logic 	is 	tx_st_data(44);		-- Byte Count Modified
	alias Cpl_CS		: std_logic_vector(2 downto 0)	is	tx_st_data(47 downto 45);	-- Completion Status Code
	alias Cpl_CompleterID	: std_logic_vector(15 downto 0)	is	tx_st_data(63 downto 48);	-- Completion ID
	alias Cpl_LowerAddr	: std_logic_vector(6 downto 0)	is	tx_st_data(70 downto 64);	-- Lower Address bits (calculated from request)
	alias Cpl_Tag		: std_logic_vector(7 downto 0)	is	tx_st_data(79 downto 72);	-- Request Identification Tag
	alias Cpl_RequesterID	: std_logic_vector(15 downto 0)	is	tx_st_data(95 downto 80);	-- Completion ID
	
	


	-- Internal Signals
	signal i_address	: std_logic_vector(63 downto 0);
	signal i_lowaddr	: std_logic_vector(6 downto 0);
	signal i_requesterID	: std_logic_vector(15 downto 0);
	signal i_tag		: std_logic_vector(7 downto 0);
	signal i_TC			: std_logic_vector(2 downto 0);
	signal DW_count		: std_logic_vector(9 downto 0);
	signal i_attr		: std_logic_vector(1 downto 0);
	
		
begin


	rd_address <= i_address;



	process (clk, reset)
	begin

		if reset = '1' then
			state <= Idle;
			tx_st_valid	<= '0';
			tx_st_sop	<= '0';
			tx_st_eop	<= '0';
			tx_st_empty	<= '0';
			tx_st_err	<= '0';

		elsif (rising_edge(clk)) then
			
			case state is
			when Idle =>
				tx_st_valid	<= '0';
				tx_st_sop	<= '0';
				tx_st_eop	<= '0';
				tx_st_empty	<= '0';
				tx_st_err	<= '0';
				
				rd_ack	 <= '0';
				
				if (MRd = '1') then
					i_address	<= address_in;
					i_lowaddr	<= address_in(6 downto 0);
					i_requesterID	<= requesterID;
					i_tag 		<= tag;
					i_TC		<= TC;
					DW_count	<= dplLength;
					i_attr		<= attr;
							
					if (address_in(2) = '1') then
						-- non-Qword aligned request... first DW of data must go out with header
						state		<= MemorySetup1;
					else
						-- Qword aligned request... header sent without data so no wait memory setup
						DW_count	<= dplLength;
						state 		<= CplD_Header;
					end if;
					next_state	<= CplD_Header;
				end if;
				

			when CplD_Header =>
				if ((unsigned(CplHdr_creds) /= 0) and (tx_st_ready = '1')) then
					TLP_Length 	<= DW_count;
					TLP_Attr	<= i_attr;	-- attribrutes same as request
					TLP_EP		<= '0';
					TLP_TD		<= '0';
					TLP_TC		<= i_TC;	-- traffic class same as request
					TLP_Type	<= "01010";	-- Completion Packet
					TLP_Fmt		<= "10";	-- 3DW, w/ data
					Cpl_ByteCount	<= DW_count & "00"; -- Effectively Multiply DW_count by 4 for Byte Count
					Cpl_BCM		<= '0';
					Cpl_CS		<= "000";	-- Successful Completion
					Cpl_CompleterID	<= x"0021";	-- Bus 0, Device 4, Function 1
					Cpl_LowerAddr	<= i_lowaddr;
					Cpl_Tag			<= i_tag;
					Cpl_RequesterID	<= i_requesterID;
					
					tx_st_valid	<= '1';
					tx_st_sop	<= '1';
					
					if (i_lowaddr(2) = '1') then
						-- Data must go out with header
						-- Set up first DW for non-Qword aligned transfer and update data count
						case i_lowaddr(3) is
						when '1' =>  -- non-Oword (128-bit) aligned
							tx_st_data(127 downto 96) <= data_to_send(127 downto 96);
						when '0' =>  -- Oword (128-bit) aligned
							tx_st_data(127 downto 96) <= data_to_send(63 downto 32);
						end case;
						
						DW_count <= std_logic_vector(unsigned(DW_count) - 1);
						rd_ack	 <= '1';
					end if;
					
					if (i_lowaddr(2) = '1') and (unsigned(DW_count) = 1) then
						-- Only 1DW being transfered w/ Header... end transaction
						tx_st_eop	<= '1';
						state <= Idle;
					else
						-- Prepare for transfering additional DW
						if (unsigned(CplData_creds) >= 4) then
							i_address <= std_logic_vector(unsigned(i_address) + 16);  -- increment address to fill memory pipeline
							state <= CplD_DataTransfer;
						else
							state <= MemorySetup1;
						end if;
					end if;
					
					next_state <= CplD_DataTransfer;
				end if;
				
			when MemorySetup2 =>
				state <= MemorySetup1;
				
			when MemorySetup1 =>
				tx_st_sop	<= '0';
				tx_st_valid <= '0';
				rd_ack	 <= '0';
				
				if ((unsigned(CplData_creds) >= 4) and (tx_st_ready = '1')) then
					i_address <= std_logic_vector(unsigned(i_address) + 16);
					state <= next_state;
				end if;

			when CplD_DataTransfer =>
				tx_st_sop	<= '0';
				tx_st_valid	<= '1';
				rd_ack	 <= '1';
				
				if (i_lowaddr(3) = '1') then
					tx_st_data(127 downto 64) <= data_to_send(63 downto 0);
					tx_st_data(63 downto 0)	<= data_to_send(127 downto 64);
				else
					tx_st_data <= data_to_send;
				end if;
				
				if (unsigned(DW_count) <= 4) then
					tx_st_eop	<= '1';
					if (unsigned(DW_count) <= 2) then
						tx_st_empty <= '1';
					end if;
					
					state <= Idle;
				else
					DW_count <= std_logic_vector(unsigned(DW_count) - 4);
					
					if ((unsigned(CplData_creds) >= 4) and (tx_st_ready = '1')) then
						i_address <= std_logic_vector(unsigned(i_address) + 16);
					else
						state <= MemorySetup1;
					end if;
				end if;
				
				
			when others =>
				state <= Idle;
			
			end case;

		end if;

	end process;

end rtl;
